#!/bin/bash

# Data files
FILE_AUX_TRAIN=./example.aux_train
FILE_TRAIN=./example.train
FILE_TEST=./example.test

# Choose from 0, 1, 2
VERBOSITY=1

# Hyperparameters
EMBED=128
HIDDEN=128
DROPOUT=0.3
DEPTH=1
BATCH=50
EPOCHS=50
SEED=559

#echo "### Training a plain encoder/decoder model"
#python3 ./mblearn/scripts/norm_advanced_seq2seq.py train $FILE_TRAIN --batch $BATCH --epochs $EPOCHS --depth $DEPTH --hidden $HIDDEN --embedding $EMBED --dropout $DROPOUT --seed $SEED --max-input 14 --max-output 14 -v 0 -V $VERBOSITY --eval $FILE_TEST

if [ -f ./example_model_att.yaml ]; then
    rm ./example_model_att.*
fi

echo "### Training a bi-directional encoder/decoder model with attention"
python3 ./mblearn/scripts/norm_advanced_seq2seq.py train $FILE_TRAIN --batch $BATCH --epochs $EPOCHS --depth $DEPTH --hidden $HIDDEN --embedding $EMBED --dropout $DROPOUT --seed $SEED --bidir --att --max-input 14 --max-output 14 -v 0 -V $VERBOSITY --prefix ./example_model_att

echo "### Evaluating using greedy search"
python3 ./mblearn/scripts/norm_advanced_seq2seq.py eval $FILE_TEST --prefix ./example_model_att -V $VERBOSITY --decoder greedy

echo "### Evaluating using beam search"
python3 ./mblearn/scripts/norm_advanced_seq2seq.py eval $FILE_TEST --prefix ./example_model_att -V $VERBOSITY --decoder beam --width 5

if [ -f ./example_model_mtl.yaml ]; then
    rm ./example_model_mtl.*
fi

echo "### Training a multi-task learning model (bi-dir enc/dec, without attention)"
python3 ./mblearn/scripts/norm_mtl_seq2seq.py train $FILE_TRAIN --batch $BATCH --epochs $EPOCHS --depth $DEPTH --hidden $HIDDEN --embedding $EMBED --dropout $DROPOUT --seed $SEED --aux $FILE_AUX_TRAIN --max-input 14 --max-output 14 --share-embeddings --share-decoder --share-encoder -v 0 -V $VERBOSITY --prefix ./example_model_mtl

echo "### Evaluating using greedy search"
python3 ./mblearn/scripts/norm_advanced_seq2seq.py eval $FILE_TEST --prefix ./example_model_mtl -V $VERBOSITY --decoder greedy

echo "### Evaluating using beam search"
python3 ./mblearn/scripts/norm_advanced_seq2seq.py eval $FILE_TEST --prefix ./example_model_mtl -V $VERBOSITY --decoder beam --width 5
