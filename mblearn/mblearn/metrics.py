from keras import backend as K

def categorical_accuracy_per_sequence(y_true, y_pred):
    return K.mean(K.min(K.equal(K.argmax(y_true, axis=-1),
                  K.argmax(y_pred, axis=-1)), axis=-1))

def sparse_categorical_accuracy_per_sequence(y_true, y_pred):
    return K.mean(K.min(K.equal(K.max(y_true, axis=-1),
                  K.cast(K.argmax(y_pred, axis=-1), K.floatx())), axis=-1))

def sparse_ca_per_sequence_drop_mask(y_true, y_pred):
    return K.mean(K.min(K.equal(K.max(y_true, axis=-1),
                  K.cast(K.argmax(y_pred, axis=-1) * K.sign(K.max(y_true, axis=-1)),
                         K.floatx())), axis=-1))

