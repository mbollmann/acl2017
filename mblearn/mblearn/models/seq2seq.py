import numpy as np
import yaml
from keras.models import Model, Sequential
from keras.layers.core import Activation, Dense, Dropout, Flatten, RepeatVector, Reshape
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import GRU, LSTM
from keras.layers.wrappers import Bidirectional, TimeDistributed

from ..helper_functions import get_sample_size, predict_classes
from ..serialization import keras, registry

def unpack_tuple(var):
    if type(var) in (list, tuple):
        return var[0], var[1]
    return var, var

def get_recurrent_unit(var):
    if var == "LSTM":
        return LSTM
    elif var == "GRU":
        return GRU
    return var

def make_simple_seq2seq(
        io_dim, io_length, embedding_dim, hidden_dim,
        bidirectional=False, broadcast_state=False, depth=2,
        dropout=0.0, recurrent_unit="LSTM", **kwargs):
    """Model for sequence-to-sequence learning using a simple encoder--decoder
    architecture.

    The encoder takes the input sequence and returns a single vector
    representation, which is then fed into the decoder at each timestep,
    which outputs a sequence again.

    Args:
        io_dim: Input/output dimensions.  Can be an integer (if the values
            match) or a tuple.
        io_length: Maximum length of input/output.  Can be an integer (if the
            values match) or a tuple.
        embedding_dim: Dimensionality of the embedding layer.
        hidden_dim: Dimensionality of the recurrent hidden layers.
        bidirectional (Optional[bool]): Whether to use bidirectional RNNs.
        depth (Optional[int]): Number of recurrent layers on encoder/decoder
            side.  Can be an integer (if the values match) or a tuple.
            Defaults to 2.
        dropout (Optional[float]): Dropout to apply between recurrent layers.
        recurrent_unit (Optional[str]): Which recurrent unit to use.
            Defaults to "LSTM".
    """
    def make_rnn(unit, dim, bidir, **kwargs):
        if bidir:
            return Bidirectional(unit(dim // 2, **kwargs))
        else:
            return unit(dim, **kwargs)

    model = Sequential()
    input_dim, output_dim = unpack_tuple(io_dim)
    input_length, output_length = unpack_tuple(io_length)
    encoder_depth, decoder_depth = unpack_tuple(depth)
    RU = get_recurrent_unit(recurrent_unit)
    model.add(Embedding(input_dim, embedding_dim,
                        mask_zero=False, input_length=input_length))
    for _ in range(encoder_depth - 1):
        model.add(make_rnn(RU, hidden_dim, bidirectional,
                           return_sequences=True, **kwargs))
        model.add(Dropout(dropout))
    model.add(make_rnn(RU, hidden_dim, bidirectional,
                       return_sequences=True, **kwargs))
    model.add(Dropout(dropout))
    #model.add(RepeatVector(output_length))
    model.add(Flatten())
    model.add(Dense(hidden_dim * output_length))
    model.add(Reshape((output_length, hidden_dim)))
    model.add(make_rnn(RU, hidden_dim, bidirectional,
                       return_sequences=True, **kwargs))
    model.add(Dropout(dropout))
    for _ in range(decoder_depth - 1):
        model.add(make_rnn(RU, hidden_dim, bidirectional,
                           return_sequences=True, **kwargs))
        model.add(Dropout(dropout))
    model.add(TimeDistributed(Dense(output_dim)))
    model.add(Activation('softmax'))
    return model


@keras.custom_object
class Seq2seq(Sequential):
    """Model for sequence-to-sequence learning.  TODO

    This model does NOT define any layers by itself.  It rather works with any
    combination of layers that have the following properties:

      + both input and output shape must be 3-dimensional and have a matching
        number of timesteps;
      + the number of timesteps must equal `input_length` + `output_length`;
      + the output at each timestep must be interpretable as class
        probabilities.

    The model then works by providing the input as the first `input_length`
    timesteps, followed by `start_symbol`, followed by `output_length`
    timesteps where the output of the previous timestep is used as the input for
    the next one, until `end_symbol` is predicted or maximum sequence length
    is reached.

    For this model, it is particularly important that input is always padded on
    the left, while output is always padded on the right, and appropriate masks
    are defined in the model's layers.  For training, **output values are
    expected to always end with** `end_symbol` (not counting padding).

    Args:
        io_length: Maximum length of input/output.  Can be an integer (if the
            values match) or a tuple.
        start_symbol: Input tensor to be used as the start symbol for output
            generation.
        end_symbol: Output tensor to be interpreted as the end-of-sequence
            symbol during prediction.
        mask_symbol: Output tensor to be used as mask.
    """

    def __init__(self, io_length=256, start_symbol=1, end_symbol=None,
                 mask_symbol=None, decoder=None):
        super(Seq2seq, self).__init__()
        self.input_length, self.output_length = unpack_tuple(io_length)
        self.start_symbol = start_symbol
        self.end_symbol = end_symbol if end_symbol is not None else [2]
        self.mask_symbol = mask_symbol if mask_symbol is not None else [0]
        self.decoder = decoder

    @property
    def input_shape(self):
        shape = super().input_shape
        return tuple([shape[0], self.input_length]) + shape[2:]

    @property
    def output_shape(self):
        shape = super().output_shape
        return tuple([shape[0], self.output_length]) + shape[2:]

    @property
    def inner_length(self):
        return self.input_length + self.output_length

    def compile(self, *args, **kwargs):
        self.has_embedding = (self.layers[0].__class__.__name__ == "Embedding")
        if len(super().input_shape) != 3 and not self.has_embedding:
            raise Exception("Seq2seq: input_shape must be 3-dimensional, unless "
                            "the first layer is an Embedding layer.")
        if len(super().output_shape) != 3:
            raise Exception("Seq2seq: output_shape must be 3-dimensional.")
        if super().input_shape[1] != super().output_shape[1]:
            raise Exception("Seq2seq: input_shape/output_shape must have "
                            "identical number of timesteps.")
        if super().input_shape[1] != self.inner_length:
            raise Exception("Seq2seq: wrong number of timesteps: found {0}, "
                            "expected input_length ({1}) + output_length ({2}) "
                            "= {3}".format(super().input_shape[1],
                                           self.input_length,
                                           self.output_length,
                                           self.inner_length))
        if kwargs.get('sample_weight_mode', 'temporal') != 'temporal':
            raise Exception("Seq2seq: sample_weight_mode must be 'temporal'.")
        kwargs['sample_weight_mode'] = 'temporal'
        return super(Seq2seq, self).compile(*args, **kwargs)

    def _assert_inner_shapes(self, x, y):
        input_shape, output_shape = super().input_shape, super().output_shape
        if input_shape[0] is not None:
            assert x.shape[0] == input_shape[0]
        assert x.shape[1] == input_shape[1]
        if len(input_shape) > 2:
            assert x.shape[2] == input_shape[2]
        if output_shape[0] is not None:
            assert y.shape[0] == output_shape[0]
        assert y.shape[1] == output_shape[1]
        if self.loss == "sparse_categorical_crossentropy":
            assert y.shape[2] == 1
        else:
            assert y.shape[2] == output_shape[2]

    def _make_sample_weights(self, x, y):
        w_in  = np.zeros((x.shape[0], self.input_length))
        w_out = np.invert(np.all(y == self.mask_symbol, axis=-1))
        w_s   = np.concatenate([w_in, w_out], axis=1)
        assert w_s.shape[0] == x.shape[0]
        assert w_s.shape[1] == super().input_shape[1]
        return w_s

    def _reshape_y(self, y):
        """Reshapes an output tensor to this model's input shape.

        Args:
            y: Numpy array in the form of the model's expected output.
        """
        if self.loss == "sparse_categorical_crossentropy":
            y = np.squeeze(y, axis=(-1,))
            if not self.has_embedding:
                y = np.identity(self.input_shape[2])[y]
        elif self.has_embedding:
            y = np.argmax(y, axis=-1)
        return y

    def _reshape_for_training(self, x, y):
        y_in = self._reshape_y(y)
        ## cut off last timestep from y_in (last timestep is either mask or
        ## <EOS>, both of which we don't need; but we need one timestep for
        ## start_symbol)
        y_in = np.delete(y_in, -1, axis=1)
        ## prepare start_symbol for concatenation with rest of input
        start_shape = (x.shape[0], 1) if len(x.shape) == 2 else (x.shape[0], 1, x.shape[2])
        start = np.broadcast_to(self.start_symbol, start_shape)
        ## combine
        new_x = np.concatenate((x, start, y_in), axis=1)
        mask_shape = (y.shape[0], self.input_length, y.shape[2])
        mask = np.broadcast_to(self.mask_symbol, mask_shape)
        new_y = np.concatenate((mask, y), axis=1)
        self._assert_inner_shapes(new_x, new_y)
        return (new_x, new_y)

    def _reshape_for_prediction(self, x):
        if len(x.shape) == 2:
            start = np.broadcast_to(self.start_symbol, (x.shape[0], 1))
            mask  = np.broadcast_to(0, (x.shape[0], self.output_length - 1))
        else:
            start = np.broadcast_to(self.start_symbol, (x.shape[0], 1, x.shape[2]))
            mask  = np.broadcast_to(0, (x.shape[0], self.output_length - 1, x.shape[2]))
        return np.concatenate((x, start, mask), axis=1)

    def fit(self, x, y, validation_data=None, **kwargs):
        x, y = np.array(x), np.array(y)
        sample_weight = self._make_sample_weights(x, y)
        x, y = self._reshape_for_training(x, y)
        if validation_data is not None:
            x_v, y_v, *w_v = validation_data
            x_v, y_v = self._reshape_for_training(x_v, y_v)
            validation_data = (x_v, y_v) + tuple(w_v)
        return super(Seq2seq, self).fit(
            x, y,
            validation_data=validation_data,
            sample_weight=sample_weight,
            **kwargs)

    def predict_classes(self, x, batch_size=32, verbose=1):
        ## prediction with supplied decoder
        x = self._reshape_for_prediction(x)
        self.model._make_predict_function()
        K_predict_function = self.model.predict_function
        self.model.predict_function = self.decoder.init(K_predict_function, self)
        preds = super().predict_classes(x, batch_size=batch_size, verbose=verbose)
        self.model.predict_function = K_predict_function
        return preds
        #return np.delete(preds, range(self.input_length + 1), axis=1)


@keras.custom_object
class Seq2seqModel(Model):
    """Model for sequence-to-sequence learning.  TODO

       Like Seq2seq, but for arbitrary graphs.
    """
    def __init__(self, *args,
                 decoder_input=1, decoder_output=0,
                 start_symbol=1, end_symbol=None,
                 mask_symbol=None, decoder=None, **kwargs):
        super(Seq2seqModel, self).__init__(*args, **kwargs)
        self.decoder_input = decoder_input
        self.decoder_output = decoder_output
        self.start_symbol = start_symbol
        self.end_symbol = end_symbol if end_symbol is not None else [2]
        self.mask_symbol = mask_symbol if mask_symbol is not None else [0]
        self.decoder = decoder

    def _prestandardize_user_data(self, x, y=None):
        """Perform basic standardization, i.e. convert x/y to list
        representations, but do not yet perform checks on model shapes
        etc. (this is done later by Model._standardize_user_data()).
        """
        from ..helper_functions import prestandardize_input_data
        if y is None:
            return prestandardize_input_data(x, self.input_names)
        return (prestandardize_input_data(x, self.input_names),
                prestandardize_input_data(y, self.output_names))

    def _standardize_decoder_specs(self):
        """Check validity of decoder input/output specifications and standardize
        them to list indices.
        """
        def f(value, names, exception_name):
            if type(value) is int:
                spec = value
            elif type(value) is str:
                try:
                    spec = names.index(value)
                except ValueError:
                    raise Exception('Unknown layer for ' + exception_name +
                                    ': ' + value + ' (can choose from: ' +
                                    ', '.join(names) + ')')
            else:
                raise Exception('Unknown layer specification for ' + exception_name +
                                ': must be list index or layer name')
            return spec
        def g(value, names, exception_name):
            if type(value) not in (list, tuple):
                value = [value]
            return [f(v, names, exception_name) for v in value]
        self.decoder_input = g(self.decoder_input, self.input_names, "decoder_input")
        self.decoder_output = g(self.decoder_output, self.output_names, "decoder_output")
        if len(self.decoder_input) != len(self.decoder_output):
            raise Exception('Number of decoder inputs must match that of '
                            'decoder outputs ({0} != {1})'.format(
                                len(self.decoder_input), len(self.decoder_output)
                            ))

    def has_decoder_embedding_at(self, i=0):
        # if decoder input is lower dim. than decoder layer, assume an Embedding layer
        input_shape = len(self.input_shape[self.decoder_input[i]])
        decoder_shape = len(self.decoder_shape[i])
        return (input_shape < decoder_shape)

    @property
    def has_decoder_embedding(self):
        return self.has_decoder_embedding_at(0)

    @property
    def decoder_io(self):
        """Return a generator of tuples (i, (d_i, d_o)) which iterates over this
           model's decoder inputs/outputs.
        """
        return enumerate(zip(self.decoder_input, self.decoder_output))

    @property
    def output_loss(self):
        if not hasattr(self, '_output_loss') or self._output_loss is None:
            from ..helper_functions import standardize_generic_input
            self._output_loss = standardize_generic_input(self.loss, self.output_names)
        return self._output_loss

    def _reshape_y(self, y, i=0):
        """Reshapes a decoder output tensor to this model's decoder input shape.

        Args:
            y: Numpy array in the form of the model's expected decoder output.
            i: Index of model decoder (if multiple; defaults to 0).
        """
        if self.output_loss[self.decoder_output[i]] == "sparse_categorical_crossentropy":
            y = np.squeeze(y, axis=(-1,))
            if not self.has_decoder_embedding_at(i):
                # TODO: does not work with 4-dim input
                y = np.identity(self.input_shape[self.decoder_input[i]][2])[y]
        elif self.has_decoder_embedding_at(i):
            y = np.argmax(y, axis=-1)
        return y

    def _make_decoder_input(self, y, i=0):
        y_in = self._reshape_y(y, i=i)
        embeds = self.has_decoder_embedding_at(i)
        ## cut off last timestep
        timestep_axis = -1 if embeds else -2
        y_in = np.delete(y_in, -1, axis=timestep_axis)
        ## concatenate with start_symbol
        start_shape = tuple(list(y_in.shape[:-1]) + [1]) if embeds else \
            tuple(list(y_in.shape[:-2]) + [1, y_in.shape[-1]])
        start = np.broadcast_to(self.start_symbol, start_shape)
        return np.concatenate((start, y_in), axis=timestep_axis)

    @property
    def decoder_shape(self):
        if not hasattr(self, '_decoder_shape'):
            raise Exception("Can only access `decoder_shape` on a compiled model")
        return self._decoder_shape

    def compile(self, *args, **kwargs):
        def validate_shapes(a, b):
            end = min(len(a), len(b))
            return a[1:end] == b[1:end]

        self._standardize_decoder_specs()
        ## TODO: shape of decoder_input must be 3-dimensional unless
        ## their first layer is an Embedding layer -- but how to check that?
        self._decoder_shape = [self.output_shape] if len(self.outputs) == 1 \
            else [self.output_shape[d_o] for d_o in self.decoder_output]
        if not all(len(d_s) >= 3 for d_s in self.decoder_shape):
            raise Exception("Seq2seq: decoder outputs must be 3-dimensional or higher.")
        for i in range(len(self.decoder_shape)):
            a = self.input_shape[self.decoder_input[i]]
            b = self.decoder_shape[i]
            if not validate_shapes(a, b):
                raise Exception("Seq2seq: decoder inputs/outputs must have "
                                "identical number of timesteps.")
        return super().compile(*args, **kwargs)

    def fit(self, x, y, validation_data=None, **kwargs):
        x, y = self._prestandardize_user_data(x, y)
        for i, (d_i, d_o) in self.decoder_io:
            x[d_i] = self._make_decoder_input(y[d_o], i=i)
        if validation_data is not None:
            x_v, y_v, *w_v = validation_data
            x_v, y_v = self._prestandardize_user_data(x_v, y_v)
            for i, (d_i, d_o) in self.decoder_io:
                x_v[d_i] = self._make_decoder_input(y_v[d_o], i=i)
            validation_data = (x_v, y_v) + tuple(w_v)
        return super().fit(
            x, y,
            validation_data=validation_data,
            **kwargs)

    def fit_generator(self, generator, *args, validation_data=None, **kwargs):
        def wrap_generator(gen):
            while True:
                inputs, targets, *sample_weights = next(gen)
                for i, (d_i, d_o) in self.decoder_io:
                    inputs[d_i] = self._make_decoder_input(targets[d_o], i=i)
                yield (inputs, targets) + tuple(sample_weights)
        if validation_data is not None:
            if type(validation_data) in (tuple, list):
                x_v, y_v, *w_v = validation_data
                x_v, y_v = self._prestandardize_user_data(x_v, y_v)
                for i, (d_i, d_o) in self.decoder_io:
                    x_v[d_i] = self._make_decoder_input(y_v[d_o], i=i)
                validation_data = (x_v, y_v) + tuple(w_v)
            else:
                validation_data = wrap_generator(validation_data)
        return super().fit_generator(
            wrap_generator(generator), *args,
            validation_data=validation_data, **kwargs
        )

    def predict(self, x, batch_size=32, verbose=1):
        """Predict with supplied decoder.

        This function makes predictions on the
        `decoder_input`/`decoder_output` model input/output, while leaving all
        other inputs/outputs unchanged from a normal :py:meth:`predict` call.
        """
        ## prediction with supplied decoder
        x = self._prestandardize_user_data(x)
        self._make_predict_function()
        K_predict_function = self.predict_function
        self.predict_function = self.decoder.init(K_predict_function, self)
        ## decoder input gets overwritten by predict_function, but it's still
        ## checked by super().predict, so make sure it is something valid before
        ## calling it
        nb_samples = get_sample_size(x)
        for d_i in self.decoder_input:
            x[d_i] = np.zeros(tuple([nb_samples] + list(self.input_shape[d_i][1:])))
        ## delegate to super() function -- decoder is doing the rest
        preds = super().predict(x, batch_size=batch_size, verbose=verbose)
        self.predict_function = K_predict_function
        return preds

    def predict_classes(self, x, **kwargs):
        """Predict classes with supplied decoder.

        This function makes class predictions on the
        `decoder_input`/`decoder_output` model input/output, while leaving all
        other inputs/outputs unchanged from a normal :py:meth:`predict` call.
        """
        preds = self.predict(x, **kwargs)
        ## since models don't have predict_classes(), we have used predict() --
        ## but this means we still need to convert to indices now
        if len(self.outputs) == 1:
            preds = predict_classes(preds)
        else:
            for d_o in self.decoder_output:
                preds[d_o] = predict_classes(preds[d_o])
        return preds



@registry.dumper(Seq2seq, 'mblearn.Seq2seq', version=1)
def _dump_seq2seq(model):
    data = yaml.load(model.to_yaml())
    data['custom_params'] = dict(
        input_length=model.input_length,
        output_length=model.output_length,
        start_symbol=model.start_symbol,
        end_symbol=model.end_symbol,
        mask_symbol=model.mask_symbol
        )
    return data

@registry.loader('mblearn.Seq2seq', version=1)
def _load_seq2seq(data, version):
    from ..serialization.keras import load_model_with_custom_params
    custom_params = data['custom_params']
    return load_model_with_custom_params(data, custom_params)

@registry.dumper(Seq2seqModel, 'mblearn.Seq2seqModel', version=1)
def _dump_seq2seq_model(model):
    data = yaml.load(model.to_yaml())
    data['custom_params'] = dict(
        start_symbol=model.start_symbol,
        end_symbol=model.end_symbol,
        mask_symbol=model.mask_symbol,
        decoder_input=model.decoder_input,
        decoder_output=model.decoder_output
        )
    return data

@registry.loader('mblearn.Seq2seqModel', version=1)
def _load_seq2seq_model(data, version):
    from ..serialization.keras import load_model_with_custom_params
    custom_params = data['custom_params']
    if 'encoder_input' in custom_params:
        del custom_params['encoder_input']
    return load_model_with_custom_params(data, custom_params)
