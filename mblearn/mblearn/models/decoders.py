import itertools as it
import math
import numpy as np
from ..helper_functions import get_sample_size

class AbstractDecoder:
    def __init__(self):
        self.filters = []
        self.function = None
        self.model = None

    def init(self, function, model):
        self.function = function
        self.model = model
        stop_symbol = model.end_symbol
        if len(stop_symbol) == 1:
            self.stop_class = stop_symbol[0]
        else:
            self.stop_class = np.argmax(stop_symbol)
        return self

    def append_filter(self, f):
        self.filters.append(f)

    def apply_filter(self, history, preds):
        for f in self.filters:
            preds = f(history, preds)
        return preds


class GreedyDecoder(AbstractDecoder):
    """Greedy decoder for Seq2seqModel model.
    """
    def __call__(self, inputs):
        assert type(inputs) in {list, tuple}
        model = self.model

        if len(model.decoder_shape) > 1:
            raise Exception("Cannot use GreedyDecoder with multi-decoder models")
        if not model.has_decoder_embedding:
            raise Exception("Models without embedding layer currently don't work with GreedyDecoder")

        # initialize variables
        decoder_shape = model.decoder_shape[0]
        nb_samples = get_sample_size(inputs)
        nb_classes = decoder_shape[-1]
        timesteps = decoder_shape[-2]
        slices = (slice(None),) * (len(decoder_shape) - 2)

        # prepare decoder input
        dec_input = np.zeros((nb_samples,) + decoder_shape[1:-1], dtype='int64')
        final_preds = np.zeros((nb_samples,) + decoder_shape[1:-1], dtype='int64')
        preds = model.start_symbol

        for i in range(timesteps):
            # set up new input & make feed-forward pass
            dec_input[slices + (i,)] = preds
            inputs[model.decoder_input[0]] = dec_input
            all_output = self.function(inputs)
            dec_output = all_output[model.decoder_output[0]]
            preds = dec_output[slices + (i, slice(None))]
            # TODO: check 4d support of filters
            preds = self.apply_filter(final_preds[slices + (slice(i),)], preds)
            # greedy prediction here
            preds = np.argmax(preds, axis=-1)
            final_preds[slices + (i,)] = preds
            # simple stop condition: have all words in batch reached <EOS>?
            # TODO: if model doesn't have fixed batch size, we could reduce
            # the batches by removing all predictions which have already
            # reached <EOS>
            if np.all(preds == self.stop_class):
                break

        # set all predictions after the first stop symbol to zero
        stop_idx = np.argmax(final_preds == self.stop_class, axis=-1)
        for i in it.product(*(range(j) for j in stop_idx.shape)):
            if stop_idx[i] == 0 and final_preds[i + (0,)] != self.stop_class:
                # oops ... we have no stop symbol in our prediction!
                final_preds[i + (-1,)] = self.stop_class
            else:
                final_preds[i + (slice(stop_idx[i]+1, None),)] = 0

        all_output[model.decoder_output[0]] = np.identity(nb_classes)[final_preds]
        return all_output


class GreedySequentialDecoder(GreedyDecoder):
    """Greedy decoder for Seq2seq model.

    At the moment, this decoder probably only works with the Seq2seq model, but
    should be generalizable without too much effort.  It currently assumes a
    model with a single input and output, as well as `input_length` and
    `output_length` class variables that specify how long the input/output parts
    are within the model's input.
    """
    def __call__(self, inputs):
        assert type(inputs) in {list, tuple}
        input_, output = inputs[0], None
        final_preds = np.zeros((input_.shape[0], self.model.output_length), dtype='int64')

        # TODO: if model is stateful, things go terribly wrong here and this
        # decoder can't be used.  I assume we'd need to copy states back and
        # forth all the time then, which we don't want to implement now.

        for j in range(self.model.output_length):
            if output is not None:
                i = j + self.model.input_length
                # greedy prediction here
                preds = np.argmax(output[:, i-1, :], axis=-1)
                final_preds[:, j-1] = preds
                # simple stop condition: have all words in batch reached <EOS>?
                # TODO: if model doesn't have fixed batch size, we could reduce
                # the batches by removing all predictions which have already
                # reached <EOS>
                if np.all(preds == self.stop_class):
                    break
                # set class predictions to be input at next timestep
                if not self.model.has_embedding:
                    preds = np.identity(input_.shape[-1])[preds]
                input_[:, i] = preds
            # assume model only has one input/output
            if self.model.uses_learning_phase:
                inputs = [input_, 0.0]
            else:
                inputs = [input_]
            output = self.function(inputs)[0]
        else:
            # one final prediction here
            preds = np.argmax(output[:, -1, :], axis=-1)
            final_preds[:, -1] = preds

        # set all predictions after the first stop symbol to zero
        stop_idx = np.argmax(final_preds == self.stop_class, axis=-1)
        for i in range(final_preds.shape[0]):
            if stop_idx[i] == 0 and final_preds[i, 0] != self.stop_class:
                # oops ... we have no stop symbol in our prediction!
                final_preds[i, -1] = self.stop_class
            else:
                final_preds[i, stop_idx[i]+1:] = 0

        return [np.identity(self.model.output_shape[-1])[final_preds]]


class BeamSearchDecoder(AbstractDecoder):
    """Beam search decoder for Seq2seqModel model.

    Args:
        beam_width (int): Beam width, defaults to 10
        length_penalty (float): Penalty to apply for each item in the sequence,
            to adjust the decoder to prefer shorter/longer sequences;
            defaults to 0.0 (no penalty)
    """
    def __init__(self, beam_width=10, length_penalty=0.0):
        super().__init__()
        self.beam_width = beam_width
        self.length_penalty = length_penalty

    def __call__(self, inputs):
        assert type(inputs) in {list, tuple}
        beam_width = self.beam_width
        model = self.model

        if len(model.decoder_shape) > 1:
            raise Exception("Cannot use BeamSearchDecoder with multi-decoder models")
        if not model.has_decoder_embedding:
            raise Exception("Models without embedding layer currently don't work with BeamSearchDecoder")

        # initialize variables
        decoder_shape = model.decoder_shape[0]
        nb_samples = get_sample_size(inputs)
        nb_classes = decoder_shape[-1]
        timesteps = decoder_shape[-2]
        slices = (slice(None),) * (len(decoder_shape) - 2)
        inst_dims = (nb_samples,) + decoder_shape[1:-2]
        beam_dims = (beam_width,) + inst_dims

        # prepare decoder input
        start_symbol = np.broadcast_to(
            np.asarray(model.start_symbol, dtype='int64'),
            inst_dims + (1,)
        )
        decoder_queue = np.zeros(beam_dims + (timesteps,), dtype='int64')
        scores = np.zeros(beam_dims)

        # When searching for the top {beam_width} predictions, we do not
        # currently look for uniqueness of classes.  If beam_width > nb_classes,
        # we *will* count some predictions more than once, and beam search will
        # not work correctly
        assert beam_width <= nb_classes
        assert timesteps > 1

        # iterate over timesteps
        for i in range(timesteps):
            # make feed-forward pass for all elements in the queue
            # & populate new_scores matrix
            new_scores = np.zeros(beam_dims + (nb_classes,))
            all_outputs = [None] * beam_width
            for j in range(beam_width):
                dq_slice = lambda x: (j,) + slices + (x,)
                decoder_input = np.concatenate(
                    (start_symbol, decoder_queue[dq_slice(slice(-1))]),
                    axis=-1
                )
                inputs[model.decoder_input[0]] = decoder_input
                all_outputs[j] = self.function(inputs)
                decoder_output = all_outputs[j][model.decoder_output[0]]
                preds = decoder_output[slices + (i, slice(None))]
                preds = self.apply_filter(decoder_queue[dq_slice(slice(i))], preds)
                scores_j = scores[j].repeat(nb_classes).reshape(preds.shape)
                # special treatment for all sequences that already reached the
                # stop symbol
                if i > 0:
                    stop_map = (decoder_queue[dq_slice(i-1)] == self.stop_class)
                    stop_map = stop_map.nonzero()
                    stop_size = stop_map[0].size
                    if stop_size > 0:
                        stop_preds = np.zeros((stop_size, nb_classes))
                        stop_preds.fill(-10000.0)
                        stop_scores = scores[(np.asarray([j] * stop_size),) + stop_map]
                        stop_preds[:, self.stop_class] = stop_scores / i
                        preds[stop_map] = stop_preds
                # update scores
                length_penalty = np.zeros(preds.shape)
                length_penalty.fill(math.exp(self.length_penalty))
                if i > 0 and stop_size:
                    length_penalty[stop_map] = 1.0
                new_scores[j] = (scores_j + preds) * length_penalty
                # special case: first timestep has only one prediction run
                if i == 0: break

            # beam search: find top {beam_width} predictions for each sample
            flat_scores = new_scores.transpose(tuple(range(1, len(inst_dims)+1)) + (0, -1))
            flat_scores = flat_scores.reshape(inst_dims + (beam_width * nb_classes,))
            top_preds = flat_scores.argsort()[slices + (slice(-beam_width, None),)]
            # make new scoring matrix
            indices = [range(z) for z in inst_dims]
            scores = [flat_scores[k + (top_preds[k],)] for k in it.product(*indices)]
            scores = np.asarray(scores).transpose().reshape(beam_dims)
            # make new decoder_queue
            if i > 0:
                beam_indices = (top_preds // nb_classes)
                decoder_queue = [decoder_queue[(beam_indices[k],) + k]
                                 for k in it.product(*indices)]
                decoder_queue = np.asarray(decoder_queue, dtype='int64')
                decoder_queue = decoder_queue.transpose(1, 0, 2)
                decoder_queue = decoder_queue.reshape(beam_dims + (timesteps,))
            top_preds = top_preds.transpose((-1,) + tuple(range(len(inst_dims))))
            decoder_queue[(slice(None),) + slices + (i,)] = top_preds % nb_classes

        best_beams = scores.argmax(axis=0)
        best_indices = np.mgrid[tuple(slice(z) for z in inst_dims)].reshape(len(inst_dims), -1)
        best_indices = (best_beams.flatten(),) + tuple(best_indices)
        best_preds = decoder_queue[best_indices].reshape(inst_dims + (-1,))

        outs = [np.zeros(z.shape) for z in all_outputs[0]]
        for idx in zip(*best_indices):
            for l, out in enumerate(all_outputs[idx[0]]):
                outs[l][idx[1:]] = out[idx[1:]]

        # set all predictions after the first stop symbol to zero
        stop_idx = np.argmax(best_preds == self.stop_class, axis=-1).reshape(inst_dims)
        for i in it.product(*(range(j) for j in stop_idx.shape)):
            if stop_idx[i] == 0 and best_preds[i + (0,)] != self.stop_class:
                # oops ... we have no stop symbol in our prediction!
                best_preds[i + (-1,)] = self.stop_class
            else:
                best_preds[i + (slice(stop_idx[i]+1, None),)] = 0

        outs[model.decoder_output[0]] = np.identity(nb_classes)[best_preds]
        return outs

        # TODO: stop condition, e.g. if all have reached EOS -- even better: we
        # could reduce loop size already if ALL samples have at least 1
        # prediction with EOS, since we could regroup the batches so all EOS's
        # are in one batch, and while we need to keep it in the loop (these
        # predictions could be thrown out be better ones!) we don't need to call
        # Keras functions on it.  MAYBE: always reorder batches so EOS sequences
        # are at the front/back, so we can easily detect when ALL in a batch
        # have gone EOS
