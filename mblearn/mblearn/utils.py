from collections import Counter, defaultdict
import itertools
import numpy as np
from .serialization import registry

class Vectorizer():
    """Converts between categorical data and NumPy vectors.

    Vectorizer stores a bi-directional mapping between hashable *items* (also
    called *symbols*) and numerical indices, and provides convenience functions
    to convert between items, indices, NumPy vectors, and sequence of those.

    Args:
        items: Collection of items the Vectorizer should recognize.
        unk_symbol (str): Symbol that is used for unknown items, i.e.,
            items that are not in this vectorizer's vocabulary.
        pad_symbol (str): Symbol that is used when padding sequences
            to a specified length.  Guaranteed to have index 0 when given.
        padding (int): Default value for :py:meth:`padding`.  Defaults to 0
            (no padding).
    """

    def __init__(self, items=None, unk_symbol=None, pad_symbol='<PAD>', padding=0):
        self._id = itertools.count()
        self._indices = {} # 0: item
        self._members = {} # item: 0
        self._padding = padding
        self._pad_symbol = None if pad_symbol is None else self.add(pad_symbol)
        self._unk_symbol = None if unk_symbol is None else self.add(unk_symbol)
        if items is not None:
            self.update(items)

    def __len__(self):
        """Return the number of contained items."""
        return len(self._indices)

    def __getitem__(self, item):
        """Synonymous with :py:meth:`get_index`."""
        return self.get_index(item)

    def __iter__(self):
        """Return an iterator over all contained items."""
        return iter(self._members)

    def __contains__(self, item):
        """Test if an item is contained in the mapping."""
        return (item in self._members)

    def __eq__(self, obj):
        if not isinstance(obj, self.__class__):
            return False
        return (self._indices == obj._indices) \
            and (self._members == obj._members) \
            and (self._pad_symbol == obj._pad_symbol) \
            and (self._unk_symbol == obj._unk_symbol)

    @property
    def padding(self):
        """int: Default padding length for sequences.

           When mapping sequences, they will be padded to this minimum length
           by default.
        """
        return self._padding

    @padding.setter
    def padding(self, size):
        if size > 0 and self._pad_symbol is None:
            raise ValueError("Cannot have padding without a pad symbol")
        self.padding = size

    def add(self, item):
        """Add an item to the map, generating a new index if necessary.

        Returns:
            The index corresponding to `item`.
        """
        if item in self:
            return
        idx = next(self._id)
        self._indices[idx] = item
        self._members[item] = idx
        return idx

    def items(self):
        """Return a list of all items."""
        return self._members.items()

    def update(self, itemlist):
        """Update the map with a collection of items."""
        for item in itemlist:
            self.add(item)

    def get_index(self, item):
        """Return a numerical index for a given item.

        Raises:
            KeyError: If `item` is not included in the map **and**
                ``unk_symbol`` is set to None; otherwise, ``unk_symbol``
                is returned.
        """
        try:
            return self._members[item]
        except KeyError:
            if self._unk_symbol is not None:
                return self._unk_symbol
            else:
                raise

    def get_item(self, index):
        """Return the item corresponding to a given index.

        Raises:
            KeyError: If index is out of range.
        """
        return self._indices[index]

    def get_onehot(self, item):
        """Return a one-hot vector for a given item.

        Equivalent to calling :py:meth:`get_index` and creating a NumPy vector
        that is 1 on the returned index and 0 everywhere else.
        """
        index = self.get_index(item)
        vector = np.zeros(len(self), dtype=np.bool)
        vector[index] = 1
        return vector

    def map_to_index(self, sequence, padding=None, pad_on="right"):
        """Map a sequence of items to a sequence of indices.

        Args:
            sequence (iterable): Items to be mapped.
            padding (Optional[int]): Minimum sequence length.
            pad_on (Optional[str]): Padding direction.  Defaults to 'right'.

        Returns:
            A list containing an index for each item in `sequence`,
            optionally padded to a given minimum length using ``pad_symbol``.
            Equivalent to calling :py:meth:`get_index` on each element of
            `sequence` and padding the resulting list.
        """
        indices = [self[item] for item in sequence]
        padding = padding if padding is not None else self._padding
        if padding > 0:
            space = padding - len(indices)
            if space < 0: # cutoff
                indices = indices[:padding]
            elif pad_on == "right":
                indices += space * [self._pad_symbol]
            else:
                indices = space * [self._pad_symbol] + indices
        return indices

    def map_to_items(self, sequence):
        """Map a sequence of indices to a sequence of items.

        Args:
            sequence (iterable): Indices to be mapped.

        Returns:
            A list containing an item for each index in `sequence`,
            minus any contained padding symbols.
            Equivalent to calling :py:meth:`get_item` on each element of
            `sequence` and removing each occurrence of ``pad_symbol``.
        """
        items = [self.get_item(idx) for idx in sequence if idx != self._pad_symbol]
        return items

    def map_to_onehot(self, sequence, padding=None, pad_on="right"):
        """Map a sequence of items to a matrix of one-hot vectors.

        Args:
            sequence (iterable): Items to be mapped.
            padding (Optional[int]): Minimum sequence length.
            pad_on (Optional[str]): Padding direction.  Defaults to 'right'.

        Returns:
            A NumPy matrix with dimensions ``(sequence_length, vocabulary_size)``
            containing one-hot representations for each item in `sequence`,
            optionally padded to a given minimum length using ``pad_symbol``.
        """
        indices = self.map_to_index(sequence, padding=padding, pad_on=pad_on)
        vectors = np.zeros((len(indices), len(self)), dtype=np.bool)
        vectors[np.arange(len(indices)), np.array(indices)] = 1
        return vectors



class Vocab:
    def __init__(self, data):
        self.freq = Counter()
        self.pair_freq = Counter()
        self.targets = defaultdict(Counter)
        self.add(data)

    def add(self, data):
        self.freq += Counter(data.words(layer=0))
        self.pair_freq += Counter(data.words(layer='all'))
        for ((source, target), freq) in self.pair_freq.items():
            self.targets[source][target] += freq

    def ambiguity(self, source):
        """Calculate ambiguity score for source token.

        The ambiguity score is defined as (1-p), where p is the token frequency
        of the most common target word for source.
        """
        target_count = len(self.targets[source])
        if target_count == 0:
            return 1.0
        elif target_count == 1:
            return 0.0
        else:
            maximum = max(self.targets[source].values())
            total = sum(self.targets[source].values())
            return (1.0 - (maximum / total))

    def knowns(self, test_data):
        """Return all source words from test_data that are in the vocabulary."""
        return [word for word in test_data.words(layer=0) if self.is_known(word)]

    def unknowns(self, test_data):
        """Return all source words from test_data that are not in the vocabulary."""
        return [word for word in test_data.words(layer=0) if self.is_unknown(word)]

    def known_pairs(self, test_data):
        """Return all source/target pairs from test_data that are in the vocabulary."""
        return [pair for pair in test_data.words(layer='all') \
                if self.is_known_pair(pair)]

    def is_known(self, source):
        return (self.freq[source] > 0)

    def is_unknown(self, source):
        return not self.is_known(source)

    def is_known_pair(self, pair):
        return (self.pair_freq[pair] > 0)


@registry.dumper(Vectorizer, 'mblearn.Vectorizer', version=1)
def _dump_vectorizer(v):
    return dict(
        padding=v._padding,
        pad_symbol=v._pad_symbol,
        unk_symbol=v._unk_symbol,
        indices=v._indices
    )

@registry.loader('mblearn.Vectorizer', version=1)
def _load_vectorizer(data, version):
    v = Vectorizer(unk_symbol=None, pad_symbol=None, padding=0)
    v._unk_symbol = data['unk_symbol']
    v._pad_symbol = data['pad_symbol']
    v._padding = data['padding']
    v._indices = data['indices']
    v._members = dict((v, k) for k, v in data['indices'].items())
    if len(data['indices']) > 0:
        v._id = itertools.count(max(v._indices) + 1)
    return v
