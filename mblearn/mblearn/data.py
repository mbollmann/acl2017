from collections import Counter
import sys

class TextData:
    """Reads and pre-processes textual data."""
    def __init__(self, data=None):
        if data is not None:
            self.read(data)

    def _reset(self):
        self._sentences = []
        self._wordfreq = Counter()

    def read(self, data):
        """Read data in one-sentence-per-line format.

        Args:
            data (str): Data to read from, as a single string.
        """
        self._reset()
        for sentence in data.split("\n"):
            sentence = sentence.strip().split()
            if sentence:
                self._sentences.append(sentence)
                self._wordfreq.update(sentence)

    def read_vertical(self, data, col=0):
        """Read data in one-word-per-line format.

        Empty lines are treated as sentence separators.

        Args:
            data (str): Data to read from, as a single string.
            col (int): If data consists of multiple columns, use this column.
                Defaults to 0 (first column).
        """
        self._reset()
        sentence = []
        for line in data.split("\n"):
            if line:
                sentence.append(line.split("\t")[col])
            else:
                self._sentences.append(sentence)
                self._wordfreq.update(sentence)
                sentence = []
        if sentence:
            self._sentences.append(sentence)
            self._wordfreq.update(sentence)

    @property
    def sentences(self):
        """Return a generator over all sentences."""
        for sent in self._sentences:
            yield sent

    @property
    def words(self):
        """Return a generator over all words."""
        for sent in self._sentences:
            for word in sent:
                yield word

    @property
    def characters(self):
        """Return a generator over all characters.

        Includes spaces between all words."""
        first = True
        for sent in self._sentences:
            for word in sent:
                if not first:
                    yield ' '
                first = False
                for char in word:
                    yield char

    @property
    def alphabet(self):
        """Return a set of all characters."""
        return set(self.characters)

    @property
    def types(self):
        """Return a set of all word types."""
        return set(self.words)

    @property
    def wordfreq(self):
        return self._wordfreq

    def _filter_types(self, filterlist, map_to):
        cutoff_map = lambda w: w if w not in filterlist else map_to
        removed_tokens = sum(self._wordfreq[token] for token in filterlist)
        self._sentences = [[x for x in map(cutoff_map, s) if x] for s in self._sentences]
        for token in filterlist:
            del self._wordfreq[token]
        self._wordfreq[map_to] += removed_tokens
        return (len(filterlist), removed_tokens)

    def frequency_cutoff(self, cutoff, map_to="<UNK>"):
        """Only keep tokens above a certain frequency.

        Args:
            cutoff (int): Remove all tokens with frequency lower or equal to
                this number.
            map_to (Optional[str]): Replace all removed tokens with this
                token.  If False, remove the tokens entirely.  Defaults to
                "<UNK>".

        Returns:
            A tuple containing the numbers of removed types and tokens.
        """
        to_remove = set(w for (w, f) in self._wordfreq.items() if f <= cutoff)
        return self._filter_types(to_remove, map_to)

    def length_cutoff(self, cutoff, map_to="<UNK>"):
        """Only keep tokens below a certain length.

        Args:
            cutoff (int): Remove all tokens with a character length higher
                than this number.
            map_to (Optional[str]): Replace all removed tokens with this
                token.  If False, remove the tokens entirely.  Defaults to
                "<UNK>".

        Returns:
            A tuple containing the numbers of removed types and tokens.
        """
        to_remove = set(w for w in self._wordfreq if len(w) > cutoff)
        return self._filter_types(to_remove, map_to)

    def replace_classes(self, digits="<NUM>", nonword="<XY>",
                        mixed_digits="<NUM_MIX>"):
        """Replace certain classes of tokens.

        All arguments can be set to None to indicate that this specific class
        should not be replaced, or to False to remove this class of tokens
        entirely.

        Args:
            digits (Optional[str]): Tokens containing all digits will be
                replaced by this string.
            nonword (Optional[str]): Tokens containing only non-alphanumeric
                characters will be replaced by this string.
            mixed_digits (Optional[str]): Tokens containing digits, but not
                exclusively so, will be replaced by this string.
        """
        if digits is not None:
            condition = set(w for w in self._wordfreq if w.isdigit())
            self._filter_types(condition, digits)
        if nonword is not None:
            condition = set(w for w in self._wordfreq if not any(c.isalnum() for c in w))
            self._filter_types(condition, nonword)
        if mixed_digits is not None:
            condition = set(w for w in self._wordfreq if any(c.isdigit() for c in w))
            self._filter_types(condition, mixed_digits)


class AnnotatedTextData:
    """Reads and preprocesses data that can have annotations."""
    def __init__(self, data=None, **kwargs):
        if data is not None:
            self.read(data, **kwargs)

    def _reset(self, cols):
        self._cols = cols
        self._sentences = []
        self._wordfreq = [Counter()] * cols

    def read(self, data, cols=None, sep="\t"):
        """Read data in one-word-per-line format.

        Each line can consist of multiple columns.  Empty lines are treated as
        sentence separators.  Lines consisting only of whitespace and/or column
        separators are treated as empty lines.

        Args:
            data (str): Data to read from, as a single string.
            cols (Optional[int]): Number of columns.  If not given, number of
                columns will be derived from the first line of input.
            sep (Optional[str]): Column separator.  Defaults to tab.
        """
        if cols is None:
            cols = data.count(sep, 0, data.index("\n")) + 1
        self._reset(cols)
        sentence = []
        for line in data.split("\n"):
            if line.strip().replace(sep, ""):
                words = tuple(line.split("\t"))
                if len(words) != cols:
                    sys.stderr.write("[AnnotatedTextData] Line has wrong number of columns: {}\n".format(line))
                    continue
                sentence.append(words)
            elif sentence:
                self._sentences.append(sentence)
                sentence = []
        if sentence:
            self._sentences.append(sentence)
        self._update_wordfreq()

    def sentences(self, layer=0):
        """Return a generator over all sentences in a given layer.

        Args:
            layer: Index of layer to return.  Can be 'all' to include
                all layers.  Defaults to 0.
        """
        if layer == "all":
            for sent in self._sentences:
                yield sent
        else:
            for sent in self._sentences:
                yield [t[layer] for t in sent]

    def words(self, layer=0):
        """Return a generator over all words in a given layer.

        Args:
            layer: Index of layer to return.  Can be 'all' to include
                all layers.  Defaults to 0.
        """
        for sent in self._sentences:
            for word in sent:
                yield (word if layer == "all" else word[layer])

    def characters(self, layer=0):
        """Return a generator over all characters in a given layer.

        Includes spaces between all words.

        Args:
            layer: Index of layer to return.  Defaults to 0.
        """
        first = True
        for sent in self._sentences:
            for word in sent:
                if not first:
                    yield ' '
                first = False
                for char in word[layer]:
                    yield char

    @property
    def sentence_count(self):
        """Return the total sentence count."""
        return len(self._sentences)

    @property
    def word_count(self):
        """Return the total word count."""
        return sum(len(sent) for sent in self._sentences)

    def types(self, layer=0):
        """Return a set of all types in a given layer.

        Args:
            layer: Index of layer to return.  Defaults to 0.
        """
        return set(self.words(layer=layer))

    def wordfreq(self, layer=0):
        return self._wordfreq[layer]

    def _update_wordfreq(self, layer="all"):
        update_range = range(self._cols) if layer == "all" else [layer]
        for i in update_range:
            self._wordfreq[i] = Counter(t[i] for s in self._sentences for t in s)

    def length_cutoff(self, cutoff, layer="any", mode="remove"):
        """Filter tokens above a certain length.

        Args:
            cutoff (int): Filter all tokens with a character length higher
                than this number.
            layer (Optional[int]): Restrict filter criterium to a given layer.
                Can be 'any' to apply when criterium matches any layer.
                Defaults to 'any'.
            mode (Optional[str]): Whether to 'remove' the long tokens, only
                'truncate' them, or replace them by 'unk'.
                Defaults to 'remove'.

        Returns:
            Nothing.
        """
        if layer == "all":
            keep = lambda t: all(len(e) <= cutoff for e in t)
        else:
            keep = lambda t: len(t[layer]) <= cutoff
        if mode == "remove":
            self._sentences = [[t for t in s if keep(t)] for s in self._sentences]
        elif mode == "truncate":
            self._sentences = [[t if keep(t) else t[:cutoff] for t in s] for s in self._sentences]
        elif mode == "unk":
            self._sentences = [[t if keep(t) else "<UNK>" for t in s] for s in self._sentences]
        else:
            raise Exception("unknown mode for length_cutoff(): {}".format(mode))
        self._update_wordfreq()

    def lower(self, layer=0):
        """Lowercase all tokens.

        Args:
            layer (Optional[int]): Only lowercase in given layer.  Can be 'all'
                to lowercase all layers.  Defaults to 0.
        """
        if layer == "all":
            self._sentences = [[tuple(e.lower() for e in t) for t in s] for s in self._sentences]
        else:
            lower_layer = lambda i, e: e.lower() if i == layer else e
            self._sentences = [[tuple(map(lower_layer, enumerate(t))) for t in s] for s in self._sentences]
        self._update_wordfreq(layer=layer)
