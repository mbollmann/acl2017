### This is just a hotfix for https://github.com/fchollet/keras/issues/3065
### until the issue is resolved in Keras itself

from keras import backend as K
from keras import activations, initializations
from keras.engine import InputSpec, Layer
from keras.layers import Merge as KerasMerge
from ..serialization import keras

@keras.custom_object
class HighwayMerge(Layer):
    """Merge two inputs in the style of a Highway layer.

    This layer combines two inputs using a weighted sum, where the weights are
    derived from the first input (the "highway connection").  If the second
    input is the output of a Dense layer applied to the first input, using
    HighwayMerge should be exactly identical to simply using the Highway layer
    (on the first input).  HighwayMerge allows us to apply different
    transformations than just a densely-connected layer to the input, before
    merging the outputs back together in a weighted fashion.
    """
    def __init__(self,
                 init='glorot_uniform',
                 activation='sigmoid',
                 weights=None,
                 bias=True,
                 bias_init='one',
                 input_dim=None,
                 **kwargs):
        self.init = initializations.get(init)
        self.activation = activations.get(activation)
        self.bias = bias
        self.bias_init = initializations.get(bias_init)
        self.initial_weights = weights
        self.input_spec = [InputSpec(ndim='2+')] * 2
        self.input_dim = input_dim
        if self.input_dim:
            kwargs['input_shape'] = (self.input_dim,)
        super(HighwayMerge, self).__init__(**kwargs)

    def build(self, input_shape):
        assert type(input_shape) == list, 'Must have exactly two inputs'
        assert len(input_shape) == 2, 'Must have exactly two inputs'
        assert input_shape[0] == input_shape[1], 'Input shapes must be identical'
        input_dim = input_shape[0][-1]
        self.input_spec = [InputSpec(dtype=K.floatx(),
                                     ndim='2+')] * 2

        self.W = self.add_weight((input_dim, input_dim),
                                 initializer=self.init,
                                 name='{}_W'.format(self.name))
        if self.bias:
            self.b = self.add_weight((input_dim,),
                                     initializer=self.bias_init,
                                     name='{}_b'.format(self.name))
        else:
            self.b = None

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, x, mask=None):
        x_orig, x_transform = x
        y = K.dot(x_orig, self.W)
        if self.bias:
            y += self.b
        carry_weight = self.activation(y)
        output = x_orig * carry_weight + x_transform * (1 - carry_weight)
        return output

    def compute_mask(self, input, input_mask=None):
        if input_mask is not None:
            assert type(input_mask) == list
            input_mask = input_mask[0]
        return input_mask

    def get_output_shape_for(self, input_shape):
        assert type(input_shape) == list, 'Must have exactly two inputs'
        assert len(input_shape) == 2, 'Must have exactly two inputs'
        return input_shape[0]

    def get_config(self):
        config = {'init': self.init.__name__,
                  'activation': self.activation.__name__,
                  'bias': self.bias,
                  'bias_init': self.bias_init.__name__,
                  'input_dim': self.input_dim}
        base_config = super(HighwayMerge, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


@keras.custom_object
class MaskedMerge(KerasMerge):
    def compute_mask(self, inputs, input_mask=None):
        if input_mask is None or not any([m is not None for m in input_mask]):
            return None

        assert hasattr(input_mask, '__len__') and len(input_mask) == len(inputs)

        if self.mode in ['sum', 'mul', 'ave']:
            masks = [K.expand_dims(m, 0) for m in input_mask if m is not None]
            return K.all(K.concatenate(masks, axis=0), axis=0, keepdims=False)
        elif self.mode == 'concat':
            masks = [K.ones_like(inputs[i]) if m is None else K.expand_dims(m) for i, m in enumerate(input_mask)]
            concatenated = K.concatenate(masks, axis=self.concat_axis)
            return K.all(concatenated, axis=-1, keepdims=False)
        elif self.mode in ['cos', 'dot']:
            return None
        elif hasattr(self.mode, '__call__'):
            if hasattr(self._output_mask, '__call__'):
                return self._output_mask(input_mask)
            else:
                return self._output_mask
        else:
            # this should have been caught earlier
            raise Exception('Invalid merge mode: {}'.format(self.mode))

def merge(inputs, mode='sum', concat_axis=-1,
          dot_axes=-1, output_shape=None, output_mask=None, name=None):
    '''Functional merge, to apply to Keras tensors (NOT layers).
    Returns a Keras tensor.
    # Example usage:
    ```python
    tensor_a = Input(shape=(32,))
    tensor_b = Input(shape=(32,))
    merged_tensor = merge([tensor_a, tensor_b], mode='concat', concat_axis=1)
    ```
    # Arguments
        mode: string or lambda/function. If string, must be one
            of: 'sum', 'mul', 'concat', 'ave', 'join', 'cos', 'dot'.
            If lambda/function, it should take as input a list of tensors
            and return a single tensor.
        concat_axis: integer, axis to use in mode `concat`.
        dot_axes: integer or tuple of integers, axes to use in mode `dot`.
        output_shape: shape tuple (tuple of integers), or lambda/function
            to compute output_shape (only if merge mode is a lambda/function).
            If the latter case, it should take as input a list of shape tuples
            (1:1 mapping to input tensors) and return a single shape tuple.
        node_indices: optional list of integers containing
            the output node index for each input layer
            (in case some input layers have multiple output nodes).
            will default to an array of 0s if not provided.
        tensor_indices: optional list of indices of output tensors
            to consider for merging
            (in case some input layer node returns multiple tensors).
    '''
    all_keras_tensors = True
    for x in inputs:
        if not hasattr(x, '_keras_history'):
            all_keras_tensors = False
            break
    if all_keras_tensors:
        input_layers = []
        node_indices = []
        tensor_indices = []
        for x in inputs:
            input_layer, node_index, tensor_index = x._keras_history
            input_layers.append(input_layer)
            node_indices.append(node_index)
            tensor_indices.append(tensor_index)
        merge_layer = MaskedMerge(input_layers, mode=mode,
                            concat_axis=concat_axis,
                            dot_axes=dot_axes,
                            output_shape=output_shape,
                            output_mask=output_mask,
                            node_indices=node_indices,
                            tensor_indices=tensor_indices,
                            name=name)
        return merge_layer.inbound_nodes[0].output_tensors[0]
    else:
        merge_layer = MaskedMerge(mode=mode,
                            concat_axis=concat_axis,
                            dot_axes=dot_axes,
                            output_shape=output_shape,
                            output_mask=output_mask,
                            name=name)
        return merge_layer(inputs)
