"""Serialization of objects.

This module provides functionality for serializing objects via the `Camel`_
library.

.. _Camel: http://camel.readthedocs.io/
"""

from camel import Camel, CamelRegistry
registry = CamelRegistry()
"""CamelRegistry: Global registry object for registering serialization
functions.
"""

_zlib_magic_headers = (
    bytes((0x78, 0x01)),
    bytes((0x78, 0x9C)),
    bytes((0x78, 0xDA))
)

def dump(obj, compress=False):
    """Dump an object.

    Args:
        obj: The object to be serialized.
        compress (Optional[bool]): If True, compress the output and return
            a byte string.  Defaults to False.

    Returns:
        A representation of `obj` as a Unicode or byte string (depending on
        the value of `compress`).
    """
    dumped = Camel([registry]).dump(obj)
    if compress:
        import zlib
        dumped = zlib.compress(dumped.encode("utf-8"))
    return dumped

def load_string(data):
    return Camel([registry]).load(data)

def load_bytes(data):
    if data[0:2] in _zlib_magic_headers:
        import zlib
        data = zlib.decompress(data)
    return load_string(data.decode("utf-8"))

def load(data):
    """Load an object.

    Args:
        data: Serialized representation of an object, in either compressed
            or uncompressed form.

    Returns:
        The un-serialized object.
    """
    if isinstance(data, str):
        return load_string(data)
    else:
        return load_bytes(data)
