from hypothesis import given, strategies as st
from hypothesis.control import assume
import string

from mblearn.data import TextData

@st.composite
def words(draw, **kwargs):
    return draw(
        st.text(
            alphabet=string.ascii_letters + string.punctuation + string.digits,
            **kwargs
        )
    )

@st.composite
def sentences(draw, **kwargs):
    return draw(st.lists(words(min_size=1, average_size=20), **kwargs))

@given(st.lists(sentences(min_size=1, max_size=20), min_size=1))
def test_properties(sentences):
    textstr = "\n".join(' '.join(s) for s in sentences)
    data = TextData(textstr)
    assert len(tuple(data.sentences)) == len(sentences)
    assert len(tuple(data.words)) == len(tuple(w for s in sentences for w in s))
    assert len(tuple(data.characters)) == len(textstr)
