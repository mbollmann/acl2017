from hypothesis import given, strategies as st
import pytest

from keras.models import Model, Sequential
from keras.layers import \
     Input, merge, Dense, Activation, Flatten, Embedding, LSTM, TimeDistributed

from mblearn.serialization import dump, load
from mblearn.models import CategoricalModelWrapper
from mblearn.utils import Vectorizer

def foobar_vectorizer():
    v = Vectorizer(items=('foo', 'bar'), unk_symbol='<UNK>', pad_symbol='<PAD>')
    return (v, len(v))

### All Keras models used in the tests are defined here.

def get_wrapper(name, loss='categorical_crossentropy'):
    (vectorizer, dim) = foobar_vectorizer()
    if name == "Dense":
        model = Sequential([
            Dense(dim, input_dim=dim),
            Activation('softmax')
        ])
    elif name == "EmbeddingDense":
        model = Sequential([
            Embedding(dim, 2, input_length=1),
            Activation('sigmoid'),
            Flatten(),
            Dense(dim),
            Activation('softmax')
        ])
    elif name == "LSTM":
        model = Sequential([
            LSTM(2 * dim, input_dim=dim, input_length=3),
            Activation('sigmoid'),
            Dense(dim),
            Activation('softmax')
        ])
    elif name == "EmbeddingLSTM":
        model = Sequential([
            Embedding(dim, 2, input_length=3),
            Activation('sigmoid'),
            LSTM(2 * dim),
            Activation('sigmoid'),
            Dense(dim),
            Activation('softmax')
        ])
    elif name == "LSTM_TDD":
        model = Sequential([
            LSTM(2 * dim, input_dim=dim, input_length=3, return_sequences=True),
            Activation('sigmoid'),
            TimeDistributed(Dense(dim)),
            Activation('softmax')
        ])
    elif name == "EmbeddingLSTM_TDD":
        model = Sequential([
            Embedding(dim, 2, input_length=3),
            Activation('sigmoid'),
            LSTM(2 * dim, return_sequences=True),
            Activation('sigmoid'),
            TimeDistributed(Dense(dim)),
            Activation('softmax')
        ])
    model.compile(optimizer='sgd', loss=loss)
    wrapper = CategoricalModelWrapper(model=model, vectorizer=vectorizer)
    return wrapper

def get_graph_wrapper(loss):
    (vectorizer, dim) = foobar_vectorizer()
    input1 = Input(shape=(3,), dtype='int32', name='my_input')
    layer = Embedding(dim, 2, input_length=3)(input1)
    layer = Activation('sigmoid')(layer)
    layer = LSTM(2 * dim, return_sequences=True)(layer)
    layer = Activation('sigmoid')(layer)
    out1 = TimeDistributed(Dense(dim))(layer)
    out1 = Activation('softmax', name='foo_output')(out1)
    out2 = LSTM(2 * dim)(layer)
    out2 = Dense(1, name='aux_output')(out2)
    model = Model(input=[input1], output=[out1, out2])
    model.compile(optimizer='sgd', loss=[loss, 'mse'])
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[vectorizer],
        output_vectorizer={'aux_output': None, 'foo_output': vectorizer}
        )
    return wrapper

def get_graph_wrapper_4d(loss):
    (vectorizer, dim) = foobar_vectorizer()
    input1 = Input(shape=(2, 3), dtype='int32', name='my_input')
    layer = TimeDistributed(Embedding(dim, 2, input_length=3))(input1)
    layer = Activation('sigmoid')(layer)
    layer = TimeDistributed(LSTM(2 * dim, return_sequences=True))(layer)
    layer = Activation('sigmoid')(layer)
    out1 = TimeDistributed(TimeDistributed(Dense(dim)))(layer)
    out1 = TimeDistributed(Activation('softmax'), name='foo_output')(out1)
    out2 = TimeDistributed(LSTM(2 * dim))(layer)
    out2 = TimeDistributed(Dense(1), name='aux_output')(out2)
    model = Model(input=[input1], output=[out1, out2])
    model.compile(optimizer='sgd', loss=[loss, 'mse'])
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[vectorizer],
        output_vectorizer={'aux_output': None, 'foo_output': vectorizer}
        )
    return wrapper


### Integration tests, testing if inputs are transformed correctly and accepted
### by the Keras models, and if outputs are what they are expected to be.

@pytest.mark.parametrize("name", ["Dense", "EmbeddingDense"])
@pytest.mark.parametrize("loss", [
    "categorical_crossentropy", "sparse_categorical_crossentropy"
])
def test_dense_model(name, loss):
    x = ['foo', 'foo', 'bar', 'baz', 'foo']
    y = ['bar', 'bar', 'foo', 'baz', 'foo']
    z = ['baz', 'bar', 'foo']
    wrapper = get_wrapper(name, loss=loss)
    wrapper.fit(x, y, nb_epoch=1, verbose=0)
    preds = wrapper.predict_classes(z, verbose=0)
    assert len(preds) == len(z)
    assert all(p in wrapper.vectorizer for p in preds)
    preds = wrapper.predict_proba(z, verbose=0)
    assert all(len(p) == len(wrapper.vectorizer) for p in preds)
    assert all(elem in wrapper.vectorizer for p in preds for (elem, _) in p)

@pytest.mark.parametrize("name", ["LSTM", "EmbeddingLSTM"])
@pytest.mark.parametrize("loss", [
    "categorical_crossentropy", "sparse_categorical_crossentropy"
])
def test_lstm_model(name, loss):
    x = [['foo', 'bar', 'baz'],
         ['bar'],
         ['foo', 'foo']]
    y = ['bar', 'bar', 'foo']
    z = [['foo', 'foo', 'foo'],
         ['baz', 'bar']]
    wrapper = get_wrapper(name, loss=loss)
    wrapper.fit(x, y, nb_epoch=1, verbose=0)
    preds = wrapper.predict_classes(z, verbose=0)
    assert len(preds) == len(z)
    assert all(p in wrapper.vectorizer for p in preds)
    preds = wrapper.predict_proba(z, verbose=0)
    assert all(len(p) == len(wrapper.vectorizer) for p in preds)
    assert all(elem in wrapper.vectorizer for p in preds for (elem, _) in p)

@pytest.mark.parametrize("name", ["LSTM_TDD", "EmbeddingLSTM_TDD"])
@pytest.mark.parametrize("loss", [
    "categorical_crossentropy", "sparse_categorical_crossentropy"
])
def test_seq2seq_model(name, loss):
    x = [['foo', 'bar', 'baz'],
         ['bar'],
         ['foo', 'foo']]
    y = [['bar', 'bar', 'foo'],
         ['foo'],
         ['bar', 'baz', 'foo']]
    z = [['foo', 'foo', 'foo'],
         ['baz', 'bar']]
    wrapper = get_wrapper(name, loss=loss)
    wrapper.fit(x, y, nb_epoch=1, verbose=0)
    preds = wrapper.predict_classes(z, verbose=0)
    assert len(preds) == len(z)
    assert all(elem in wrapper.vectorizer for p in preds for elem in p)
    preds = wrapper.predict_proba(z, verbose=0)
    assert len(preds) == len(z)
    assert all(len(p) == 3 for p in preds)
    assert all(len(elem) == len(wrapper.vectorizer) for p in preds for elem in p)
    wrapper.categorical_output = False
    preds = wrapper.predict(z, verbose=0)
    assert preds.shape == (len(z), 3, len(wrapper.vectorizer))

@pytest.mark.parametrize("loss", [
    "categorical_crossentropy", "sparse_categorical_crossentropy"
])
def test_graph_model(loss):
    x = [['foo', 'bar', 'baz'],
         ['bar'],
         ['foo', 'foo']]
    y1 = [['bar', 'bar', 'foo'],
          ['foo'],
          ['bar', 'baz', 'foo']]
    y2 = [[1], [0], [1]]
    z = [['foo', 'foo', 'foo'],
         ['baz', 'bar']]
    wrapper = get_graph_wrapper(loss)
    wrapper.fit([x], {'aux_output': y2, 'foo_output': y1}, nb_epoch=1, verbose=0)
    preds = wrapper.predict_classes([z], verbose=0)
    assert len(preds) == 2
    assert len(preds[0]) == len(z)
    assert preds[1].shape[0] == len(z)
    assert all(elem in wrapper.output_vectorizer[0] for p in preds[0] for elem in p)
    preds = wrapper.predict_proba([z], verbose=0)
    assert len(preds) == 2
    assert all(len(p) == 3 for p in preds[0])
    assert all(len(elem) == len(wrapper.output_vectorizer[0]) for p in preds[0] for elem in p)

def test_embedding_detection():
    wrapper = get_graph_wrapper("categorical_crossentropy")
    assert wrapper.has_embedding_at(0)
    wrapper = get_graph_wrapper_4d("categorical_crossentropy")
    assert wrapper.has_embedding_at(0)

@pytest.mark.parametrize("loss", [
    "categorical_crossentropy", "sparse_categorical_crossentropy"
])
def test_graph_model_4d(loss):
    x = [[['foo', 'bar', 'baz'],
          ['bar']],
         [['foo', 'foo']]]
    y1 = [[['bar', 'bar', 'foo'],
           ['foo']],
          [['bar', 'baz', 'foo']]]
    y2 = [[[1], [0]], [[1], [0]]]
    z = [[['foo', 'foo', 'foo'],
          ['baz', 'bar']],
         [['foo']]]
    wrapper = get_graph_wrapper_4d(loss)
    wrapper.fit([x], {'aux_output': y2, 'foo_output': y1}, nb_epoch=1, verbose=0)
    preds = wrapper.predict_classes([z], verbose=0)
    assert len(preds) == 2
    assert len(preds[0]) == len(z)
    assert preds[1].shape[0] == len(z)
    for preds_0 in preds[0]:
        assert all(elem in wrapper.output_vectorizer[0] for p in preds_0 for elem in p)
    preds = wrapper.predict_proba([z], verbose=0)
    assert len(preds) == 2
    assert all(len(p) == 2 for p in preds[0])
    assert all(len(p) == 3 for preds_0 in preds[0] for p in preds_0)
    for preds_0 in preds[0]:
        assert all(len(elem) == len(wrapper.output_vectorizer[0]) for p in preds_0 for elem in p)
