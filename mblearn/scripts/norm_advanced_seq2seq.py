#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import sys

from boltons.iterutils import split as bsplit

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params, \
     filter_unknowns
from norm_vocab import Vocab

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def append_end_symbol(wordlist, symbol='<END>'):
    return tuple([c for c in word] + [symbol] for word in wordlist)

def convert_to_xy(data, batch_size=None,
                  for_decoder_input=False,
                  sentence_input=False):
    if not sentence_input:
        X = list(data.words(0))
        Y = data.words(1)
    else:
        X = ['_'.join(words) for words in data.sentences(0)]
        Y = ['_'.join(words) for words in data.sentences(1)]
    if for_decoder_input:
        y = tuple([for_decoder_input] + [c for c in word] for word in Y)
    else:
        y = append_end_symbol(Y)
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def count_correct(preds, truths):
    return sum(p == t for p, t in zip(preds, truths))

def count_correct_charwise(preds, truths):
    correct, total = 0, 0
    for pred, truth in zip(preds, truths):
        correct += sum(p == t for (p, t) in zip(pred, truth))
        total   += max((len(pred), len(truth)))
    return correct, total

def build_model(args, enc_vec, dec_vec, enc_len, dec_len):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation
    from keras.optimizers import Adam
    from mblearn.layers import merge
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper
    from encdec import encdec

    ### build inputs
    enc_input = Input(shape=(enc_len,), dtype='int32', name='encoder_input')
    enc_embed = Embedding(len(enc_vec), args.embedding, mask_zero=True)(enc_input)
    dec_input = Input(shape=(dec_len,), dtype='int32', name='decoder_input')
    dec_embed = Embedding(len(dec_vec), args.embedding, mask_zero=True)(dec_input)

    ### build encoder/decoder
    if args.ensembles > 1:
        dec_layers = []
        for _ in range(args.ensembles):
            dec_layer = encdec(args.hidden, enc_embed, dec_embed,
                               bi_enc=args.bidir, depth=args.depth,
                               dropout_W=args.dropout,
                               recurrent_unit=args.recurrent_unit,
                               with_attention=args.att)
            dec_layers.append(dec_layer)
        dec_layer = merge(dec_layers, mode='ave')
    else:
        dec_layer = encdec(args.hidden, enc_embed, dec_embed,
                           bi_enc=args.bidir, depth=args.depth,
                           dropout_W=args.dropout,
                           recurrent_unit=args.recurrent_unit,
                           with_attention=args.att)

    ### prediction layer
    dec_layer = TimeDistributed(Dense(len(dec_vec)))(dec_layer)
    dec_output = Activation('softmax', name='decoder_output')(dec_layer)

    ### build model
    model = Seq2seqModel(
        input=[enc_input, dec_input], output=[dec_output],
        decoder_input=1, decoder_output=0,
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
    )
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=Adam(lr=0.003, clipnorm=1.),
        metrics={'decoder_output': Wacc}
    )
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[enc_vec, None],
        output_vectorizer=[dec_vec]
    )
    return wrapper

def evaluate_model(wrapper, args, infile=None):
    def print_accuracy(name, preds, truth):
        Cc, Ct = count_correct_charwise(preds, truth)
        print(name)
        print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
        correct = count_correct(preds, truth)
        print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds),
                                                             correct / len(preds)))

    if infile is None:
        infile = args.infile
    sys.stderr.write("Evaluating on: {0}\n".format(infile.name))
    data = get_data_from_file(infile)
    sys.stderr.write("Found {} words.\n".format(data.word_count))
    X, y = convert_to_xy(data, sentence_input=args.sentence_input)
    preds = wrapper.predict_classes([X, None], batch_size=args.batch, verbose=args.verbose)[0]
    if hasattr(args, 'print') and args.print:
        if args.sentence_input:
            for p_sent in preds:
                assert p_sent[-1] == "<END>"
                for p in bsplit(p_sent[:-1], sep='_'):
                    print(''.join(p))
                print('')
        else:
            sentence_boundaries = set()
            offset = -1
            for s in data.sentences(0):
                offset += len(s)
                sentence_boundaries.add(offset)
            for i, p in enumerate(preds):
                assert p[-1] == "<END>"
                print(''.join(p[:-1]))
                if i in sentence_boundaries:
                    print('')
    else:
        if args.sentence_input:
            print(("Caution: There is no trivial way to convert sentence output back\n"
                   "         to word output, so 'word accuracy' will actually mean\n"
                   "         'sentence accuracy' here."), file=sys.stderr)
        print_accuracy("Seq2seq", preds, y)
        traindata = None
        if args.command == "train":
            args.infile.seek(0)
            traindata = get_data_from_file(args.infile)
        elif args.trainfile:
            traindata = get_data_from_file(args.trainfile)
        if traindata is not None:
            train_X, train_y = convert_to_xy(traindata)
            vocab = Vocab(train_X, train_y)
            # Greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=1.0)
            print_accuracy("Mapper (greedy: c>=0, a<=1) + Seq2seq", preds_map, y)
            # Semi-greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.75)
            print_accuracy("Mapper (semi-g: c>=0, a<=0.75) + Seq2seq", preds_map, y)
            # Strict mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.0)
            print_accuracy("Mapper (strict: c>=0, a==0) + Seq2seq", preds_map, y)

def train(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    enc_vectorizer.update(data.characters(0))
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(data.characters(1))
    dec_vectorizer.update(('<START>', '<END>'))

    if args.pretrain:
        pretrain_data = get_data_from_file(args.pretrain,
                                           cutoff=(args.max_input, args.max_output))
        enc_vectorizer.update(pretrain_data.characters(0))
        dec_vectorizer.update(pretrain_data.characters(1))

    if args.sentence_input:
        enc_vectorizer.update('_')
        dec_vectorizer.update('_')

    wrapper = build_model(
        args, enc_vectorizer, dec_vectorizer,
        args.max_input, args.max_output
    )
    X, y = convert_to_xy(data, sentence_input=args.sentence_input)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        X_v, y_v = convert_to_xy(v_data, sentence_input=args.sentence_input)
        params = {'validation_data': ([X_v, None], [y_v])}
    else:
        params = {'validation_split': args.validation_split}

    if args.pretrain:
        pre_X, pre_y = convert_to_xy(pretrain_data, sentence_input=args.sentence_input)
        epochs, args.epochs = args.epochs, args.pretrain_epochs
        train_model(wrapper, [pre_X, None], [pre_y], args, validation_split=0.0)
        args.epochs = epochs

    history = train_model(wrapper, [X, None], [y], args, **params)

    if args.prefix:
        save_model(wrapper, args.prefix)
        save_history(history, args.prefix)

    if args.eval is not None:
        wrapper.model.decoder = GreedyDecoder()
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.decoders import GreedyDecoder, BeamSearchDecoder
    from mblearn.models.filters import DictionaryFilter
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'decoder_output': Wacc}
    )
    if args.decoder == 'greedy':
        wrapper.model.decoder = GreedyDecoder()
    elif args.decoder == 'beam':
        wrapper.model.decoder = BeamSearchDecoder(beam_width=args.width)
    if args.filter:
        words = [w.strip() for w in args.filter.readlines()]
        vectorizer = wrapper.output_vectorizer[wrapper.model.decoder_output[0]]
        eos_idx = wrapper.model.end_symbol[0]
        sys.stderr.write("Building dictionary...")
        dictfilt = DictionaryFilter(words, vectorizer, eos_idx)
        sys.stderr.write(" done.\n")
        wrapper.model.decoder.append_filter(dictfilt)

    evaluate_model(wrapper, args)

def embed(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, HiddenStateLSTM, MaskedMerge, merge, TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    from pprint import pprint

    wrapper = load_model(args.prefix)
    model = wrapper.model

    if not args.words:
        embedding = model.get_layer(name="embedding_1")
        embeds, *_ = embedding.get_weights()
        for n in range(embeds.shape[0]):
            char = wrapper.input_vectorizer[0].get_item(n)
            weights = [str(w) for w in embeds[n]]
            print("\t".join((char, ",".join(weights))))

    else:
        model.compile(loss="sparse_categorical_crossentropy", optimizer="adam")
        enc_in  = model.get_layer(name="encoder_input")
        if enc_in is None:
            enc_in = model.get_layer(name="main_enc_input")
        enc_in = enc_in.input
        enc_out = model.get_layer(name="hiddenstatelstm_1").output
        get_output = K.function([enc_in, K.learning_phase()], enc_out)
        X, _ = wrapper.transform_inputs([list(args.words), None], None)
        embeds, *_ = get_output([X[0], False])
        for n in range(embeds.shape[0]):
            word = str(args.words[n])
            weights = [str(w) for w in embeds[n]]
            print("\t".join((word, ",".join(weights))))


def pratt(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, HiddenStateLSTM, MaskedMerge, merge, TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    if args.plot:
        import matplotlib
        matplotlib.use('Agg')
        import seaborn as sns

    wrapper = load_model(args.prefix)
    for n, layer in enumerate(wrapper.model.layers):
        if isinstance(layer, AttentionLSTM):
            attn_idx = n
            break
    else:
        print(wrapper.model.layers)
        print("Couldn't find attentional layer!")
        exit(1)

    wrapper.model.layers[attn_idx].output_alpha = True
    # hack to make "output_alpha" take effect... there must be a better way
    wrapper.model = load(dump(wrapper.model))
    wrapper.model.load_weights(args.prefix + '.h5')
    wrapper.model = Model(input=wrapper.model.inputs, output=wrapper.model.layers[attn_idx].output)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam"
    )

    print("Generating attention weights from: {0}".format(args.infile.name))
    data = get_data_from_file(args.infile)
    X, y = convert_to_xy(data, for_decoder_input='<START>')
    wrapper.input_vectorizer[1] = wrapper.output_vectorizer[0]
    #wrapper.input_padding = ['left', 'right']
    preds = wrapper.predict([X, y], batch_size=args.batch, verbose=args.verbose)

    for i, (p_i, x_i, y_i) in enumerate(zip(preds, X, y)):
        weight_map = p_i[-len(y_i):, -len(x_i):].T
        y_i = y_i[1:] + ['<END>']
        if args.csv:
            args.csv.write("\t" + "\t".join(y_i) + "\n")
            for j, x_j in enumerate(x_i):
                args.csv.write(x_j + "\t")
                weight_row = ["{0:.4f}".format(e) for e in weight_map[j]]
                args.csv.write("\t".join(weight_row) + "\n")
        if args.plot:
            ax = sns.heatmap(weight_map[:, ::-1],
                             xticklabels=y_i,
                             yticklabels=x_i[::-1],
                             square=True, robust=True,
                             cbar=False)
            fig = ax.get_figure()
            fig.savefig(args.plot + str(i) + ".png")


if __name__ == "__main__":
    description = "Normalization using encoder-decoder architectures."
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    subparsers = parser.add_subparsers(title="available commands", dest='command')

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--att',
                         action='store_true',
                         default=False,
                         help='Use attentional decoders')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--ensembles',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Train an ensemble of N enc/decs')
    p_train.add_argument('--pretrain',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         help='File for pretraining the model')
    p_train.add_argument('--pretrain-epochs',
                         metavar='N',
                         type=int,
                         default=0,
                         help=('Number of epochs for pretraining (default: '
                               'same as --epochs)'))
    p_train.add_argument('--sentence-input',
                         action='store_true',
                         default=False,
                         help='Use full sentences as input, not words')

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-d', '--decoder',
                        choices=('greedy','beam'),
                        default='greedy',
                        help='Decoder to use (default: %(default)s)')
    p_eval.add_argument('-w', '--width',
                        type=int,
                        default=5,
                        help='Beam width when using "--decoder beam" (default: %(default)i)')
    p_eval.add_argument('-f', '--filter',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File with words for a dictionary filter')
    p_eval.add_argument('--sentence-input',
                        action='store_true',
                        default=False,
                        help='Use full sentences as input, not words')
    p_eval.add_argument('--print',
                        action='store_true',
                        default=False,
                        help='Print predictions instead of scores')
    p_eval.add_argument('--trainfile',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File the model was trained on; used for extra evaluations')

    p_att = subparsers.add_parser('pratt')
    p_att.set_defaults(func=pratt,
                       help="Print attention weights")
    p_att.add_argument('--csv',
                       metavar='FILE',
                       type=argparse.FileType('w', encoding="UTF-8"),
                       default=sys.stdout,
                       help='Save to file as CSV (default: <stdout>)')
    p_att.add_argument('--plot',
                       metavar='FILEPREFIX',
                       type=str,
                       default=None,
                       help='Save plots as FILEPREFIX{n}.png')
    add_common_eval_params(p_att)

    p_emb = subparsers.add_parser('embed')
    p_emb.set_defaults(func=embed,
                       help="Extract embeddings")
    p_emb.add_argument('-s', '--seed',
                       metavar='N',
                       type=int,
                       default=0,
                       help='Seed for RNG (default: 0=none)')
    p_emb.add_argument('-w', '--words',
                       type=str,
                       nargs='+',
                       help='Words to generate embeddings for')
    p_emb.add_argument('-x', '--prefix',
                       metavar='STR',
                       type=str,
                       required=True,
                       help='Prefix for loading the model (required)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    if args.command == "eval" and args.print:
        args.verbose = 0
    if args.command == "train" and args.pretrain_epochs == 0:
        args.pretrain_epochs = args.epochs

    args.func(args)
