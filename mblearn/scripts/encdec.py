"""Functions to build and apply encoder/decoder layers.

Building the layers (= returning a list of layer objects) and applying them (to
an input, returning a tensor) is separated to make it easier to build models
with shared layers.

In theory, it would be best to just build a Model for each encoder/decoder,
which can than be easily applied to any input, however this doesn't currently
work with masking.
"""

from keras.layers import *
from mblearn.layers import \
     AttentionLSTM, HiddenStateLSTM, HiddenStateGRU, HiddenStateSimpleRNN, \
     merge, BidirectionalHidden, HiddenToCellStateLSTM

def get_recurrent_unit(ru):
    if ru == "LSTM":
        return HiddenStateLSTM
    elif ru == "htcLSTM":
        return HiddenToCellStateLSTM
    elif ru == "GRU":
        return HiddenStateGRU
    else:
        return HiddenStateSimpleRNN

def build_encoder_layers(hidden_dim, bidirectional=False, depth=2,
                         recurrent_unit="LSTM", return_sequences=False, **kwargs):
    layers = []
    RNN = get_recurrent_unit(recurrent_unit)
    if bidirectional:
        if type(bidirectional) is bool:
            bidirectional = 'concat'
        if bidirectional == 'concat':
            hidden_dim //= 2
    for n in range(depth, 0, -1):
        rs = (n>1) or return_sequences
        rnn = RNN(hidden_dim, return_sequences=rs, **kwargs)
        if bidirectional:
            rnn = BidirectionalHidden(rnn, merge_mode=bidirectional)
        layers.append(rnn)
    return layers

def build_decoder_layers(hidden_dim, depth=2, recurrent_unit="LSTM",
                         with_attention=False, **kwargs):
    layers = []
    RNN = get_recurrent_unit(recurrent_unit)
    for i in range(depth):
        if with_attention and i == 0:
            if recurrent_unit != "LSTM":
                raise Exception("Attentional decoder currently only supported with LSTMs")
            layer = AttentionLSTM(hidden_dim, return_sequences=True, **kwargs)
        else:
            layer = RNN(hidden_dim, return_sequences=True, **kwargs)
        layers.append(layer)
    return layers

def encode(enc_input, layers, return_all_layers=False, hidden_states=None):
    enc_layer = enc_input
    enc_layers, hidden_out = [], []
    for n, layer in enumerate(layers):
        if hidden_states:
            layer_input = [enc_layer] + hidden_states[n]
        else:
            layer_input = enc_layer
        if isinstance(layer, list):
            sub_outs, sub_hiddens = [], []
            for sublayer in layer:
                so, *sh = sublayer(layer_input)
                sub_outs.append(so)
                sub_hiddens.append(sh)
            enc_layer = merge(sub_outs, mode='concat', concat_axis=-1)
            hidden = [merge(h, mode='ave') for h in zip(*sub_hiddens)]
        else:
            enc_layer, *hidden = layer(layer_input)
        enc_layers.append(enc_layer)
        hidden_out.append(hidden)
    if return_all_layers:
        return (enc_layers, hidden_out)
    else:
        return (enc_layer, hidden_out)

def decode(dec_input, layers, attend=None, hidden_states=None):
    dec_layer = dec_input
    for n, layer in enumerate(layers):
        if attend and len(attend) > n:
            layer_input = [dec_layer, attend[n]]
        elif hidden_states:
            layer_input = [dec_layer] + hidden_states[n]
        else:
            layer_input = dec_layer
        dec_layer = layer(layer_input)
        if isinstance(dec_layer, (tuple, list)):
            dec_layer = dec_layer[0]
    return dec_layer

def encoder(hidden_dim, enc_input, hidden_states=None, **kwargs):
    layers = build_encoder_layers(hidden_dim, **kwargs)
    return encode(enc_input, layers, hidden_states=hidden_states)

def decoder(hidden_dim, dec_input, hidden_states=None, **kwargs):
    layers = build_decoder_layers(hidden_dim, **kwargs)
    return decode(dec_input, layers, hidden_states=hidden_states)

def encdec(hidden_dim, enc_input, dec_input, bi_enc=False,
           recurrent_unit="LSTM", with_attention=False,
           start_hidden=None,
           **kwargs):
    if with_attention:
        enc_layers = build_encoder_layers(hidden_dim, bidirectional=bi_enc,
                                          recurrent_unit=recurrent_unit,
                                          return_sequences=True, **kwargs)
        encoded, enc_hidden = encode(enc_input, enc_layers, hidden_states=start_hidden)
        dec_layers = build_decoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          with_attention=True, **kwargs)
        return decode(dec_input, dec_layers, attend=[encoded], hidden_states=None)
    else:
        enc_layers = build_encoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          bidirectional=bi_enc, **kwargs)
        _, enc_hidden = encode(enc_input, enc_layers, hidden_states=start_hidden)
        dec_layers = build_decoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          **kwargs)
        return decode(dec_input, dec_layers, hidden_states=enc_hidden)
