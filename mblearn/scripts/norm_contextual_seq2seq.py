#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import itertools as it
import numpy as np
import sys

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params, \
     filter_unknowns
from norm_vocab import Vocab

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def append_end_symbol(sentlist, symbol='<END>', as_words=False):
    if as_words:
        return tuple([c for c in word] + [symbol] for word in sentlist)
    else:
        return tuple(tuple([c for c in word] + [symbol] for word in wordlist) \
                     for wordlist in sentlist)

def convert_to_xy(data, batch_size=None, for_decoder_input=False, as_words=False):
    fetch = data.words if as_words else data.sentences # using sentences = 4d
    X = list(fetch(0))
    if for_decoder_input:
        dec_sym = for_decoder_input
        if as_words:
            y = tuple([dec_sym] + [c for c in word] for word in fetch(1))
        else:
            y = tuple(tuple([dec_sym] + [c for c in word] for word in wordlist) \
                      for wordlist in fetch(1))
    else:
        y = append_end_symbol(fetch(1), as_words=as_words)
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def count_correct(preds, truths):
    return sum(p == t for p, t in zip(preds, truths))

def count_correct_charwise(preds, truths):
    correct, total = 0, 0
    for pred, truth in zip(preds, truths):
        correct += sum(p == t for (p, t) in zip(pred, truth))
        total   += max((len(pred), len(truth)))
    return correct, total

def flatten(*lists):
    assert all(len(lists[0]) == len(list_) for list_ in lists)
    flattened = [words for sentences in zip(*lists) \
                 for words in it.zip_longest(*sentences, fillvalue='') \
                 if words[0] != '']
    return zip(*flattened)

def build_model(args, enc_vec, dec_vec, enc_len, dec_len):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation, Reshape
    from keras.optimizers import Adam
    from mblearn.layers import merge, TimeDistributedMulti, HighwayMerge
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper
    from encdec import build_encoder_layers, build_decoder_layers, encode, decode

    ##### TODO: DEPTH FIXED TO 1 AT THE MOMENT

    ### build inputs
    char_enc_input = Input(shape=(enc_len,), dtype='int32', name='encoder_input')
    char_dec_input = Input(shape=(enc_len,), dtype='int32', name='decoder_input')
    char_enc_embed_layer = Embedding(len(enc_vec), args.embedding, mask_zero=True)
    char_enc_embed_tensor = char_enc_embed_layer(char_enc_input)
    char_dec_embed_layer = Embedding(len(dec_vec), args.embedding, mask_zero=True)
    char_dec_embed_tensor = char_dec_embed_layer(char_dec_input)

    word_enc_input = Input(shape=(args.max_sequence, enc_len), dtype='int32', name='word_encoder_input')
    word_enc_embed_layer = TimeDistributed(char_enc_embed_layer)
    word_enc_embed_tensor = word_enc_embed_layer(word_enc_input)
    word_dec_input = Input(shape=(args.max_sequence, dec_len), dtype='int32', name='word_decoder_input')
    word_dec_embed_layer = TimeDistributed(char_dec_embed_layer)
    word_dec_embed_tensor = word_dec_embed_layer(word_dec_input)

    ### char-based model -- this time it's 4d!
    ### build encoder/decoder
    char_enc_layers = build_encoder_layers(args.hidden, recurrent_unit=args.recurrent_unit,
                                           bidirectional=args.bidir, depth=args.depth,
                                           dropout_W=args.dropout)
    word_enc_layers = [TimeDistributedMulti(layer) for layer in char_enc_layers]
    word_enc_tensor, _ = encode(word_enc_embed_tensor, word_enc_layers)

    char_dec_layers = build_decoder_layers(args.hidden, recurrent_unit=args.recurrent_unit,
                                           depth=args.depth, dropout_W=args.dropout)
    word_dec_layers = [TimeDistributedMulti(layer) for layer in char_dec_layers]

    # here we inject the word context dependency
    word_layers = build_encoder_layers(args.hidden, recurrent_unit=args.recurrent_unit,
                                       bidirectional='ave', depth=args.depth_sequence,
                                       dropout_W=args.dropout_sequence, return_sequences=True)
    word_tensors, _ = encode(word_enc_tensor, word_layers)
    word_tensors = TimeDistributedMulti(HighwayMerge())([word_enc_tensor, word_tensors])

    # and back to the decoder again...
    word_hidden = [[word_tensors]] * len(char_dec_layers)
    word_dec_tensor = decode(word_dec_embed_tensor, word_dec_layers, hidden_states=word_hidden)
    char_dec_dense_layer = TimeDistributed(Dense(len(dec_vec)))
    word_dec_dense_layer = TimeDistributed(char_dec_dense_layer)
    word_dec_tensor = word_dec_dense_layer(word_dec_tensor)
    word_dec_tensor = Reshape((args.max_sequence * dec_len, len(dec_vec)))(word_dec_tensor)
    word_dec_output = Activation('softmax')(word_dec_tensor)
    word_dec_output = Reshape((args.max_sequence, dec_len, len(dec_vec)), name='word_decoder_output')(word_dec_output)

    ### 3d model
    if args.twostage:
        char_enc_tensor, _ = encode(char_enc_embed_tensor, char_enc_layers)
        char_hidden = [[char_enc_tensor]] * len(char_dec_layers)
        char_dec_tensor = decode(char_dec_embed_tensor, char_dec_layers, hidden_states=char_hidden)
        char_dec_tensor = char_dec_dense_layer(char_dec_tensor)
        char_dec_output = Activation('softmax', name='decoder_output')(char_dec_tensor)

        c_model = Seq2seqModel(
            input=[char_enc_input, char_dec_input], output=[char_dec_output],
            decoder_input=1, decoder_output=0,
            start_symbol=dec_vec['<START>'],
            end_symbol=[dec_vec['<END>']],
            mask_symbol=[0]
        )
        c_model.compile(
            loss="sparse_categorical_crossentropy",
            optimizer=Adam(lr=0.003),
            metrics={'decoder_output': Wacc}
        )
        c_wrapper = CategoricalModelWrapper(
            model=c_model,
            input_vectorizer=[enc_vec, None],
            output_vectorizer=[dec_vec]
        )

        for layer in char_enc_layers:
            layer.trainable = False
        for layer in char_dec_layers:
            layer.trainable = False
        char_dec_dense_layer.trainable = False
        char_enc_embed_layer.trainable = False
        char_dec_embed_layer.trainable = False

    else:
        c_wrapper = None

    w_model = Seq2seqModel(
        input=[word_enc_input, word_dec_input], output=[word_dec_output],
        decoder_input=1, decoder_output=0,
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
    )
    w_model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=Adam(lr=0.003),
        metrics={'word_decoder_output': Wacc} # 4-dim Wacc! (but might work as it is already?)
    )
    w_wrapper = CategoricalModelWrapper(
        model=w_model,
        input_vectorizer=[enc_vec, None],
        output_vectorizer=[dec_vec]
    )

    return c_wrapper, w_wrapper

def evaluate_model(wrapper, args, infile=None):
    def print_accuracy(name, preds, truth):
        Cc, Ct = count_correct_charwise(preds, truth)
        print(name)
        print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
        correct = count_correct(preds, truth)
        print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds),
                                                             correct / len(preds)))

    if infile is None:
        infile = args.infile
    sys.stderr.write("Evaluating on: {0}\n".format(infile.name))
    data = get_data_from_file(infile)
    sys.stderr.write("Found {} sentences, {} words.\n".format(data.sentence_count, data.word_count))
    X, y = convert_to_xy(data)

    # split up too long sentences
    new_Xy = []
    split_indices = []
    maxlen = wrapper.input_shape[0][1]
    toolong = 0
    for x_, y_ in zip(X, y):
        assert len(x_) == len(y_)
        if len(x_) > maxlen:
            i = 0
            toolong += 1
            while i < len(x_):
                if i > 0:
                    split_indices.append(len(new_Xy) - 1)
                new_Xy.append((x_[i:i+maxlen], y_[i:i+maxlen]))
                i += maxlen
        else:
            new_Xy.append((x_, y_))
    if toolong > 0:
        sys.stderr.write("Caution: {} sentences were split up due to being too long!\n".format(toolong))
    X, y = zip(*new_Xy)

    preds = wrapper.predict_classes([X, None], batch_size=args.batch, verbose=args.verbose)[0]
    # CAREFUL: Each sentence in `preds` will have len==batch_size.  We have to
    # explicitly discard the additional word timesteps!  The code below always
    # does this by zip()ing with X in some way.
    if hasattr(args, 'print') and args.print:
        for i, (sent, sent_orig) in enumerate(zip(preds, X)):
            for p, _ in zip(sent, sent_orig):
                assert p[-1] == "<END>"
                print(''.join(p[:-1]))
            if i not in split_indices:
                print('') # sentence separator
    else:
        # since we don't care about sentence-level scores, we just flatten the
        # lists and evaluate as usual
        X, y, preds = flatten(X, y, preds)
        print_accuracy("Seq2seq", preds, y)
        traindata = None
        if args.command == "train":
            args.infile.seek(0)
            traindata = get_data_from_file(args.infile)
        elif args.trainfile:
            traindata = get_data_from_file(args.trainfile)
        if traindata is not None:
            train_X, train_y = convert_to_xy(traindata, as_words=True)
            vocab = Vocab(train_X, train_y)
            # Greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=1.0)
            print_accuracy("Mapper (greedy: c>=0, a<=1) + Seq2seq", preds_map, y)
            # Semi-greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.75)
            print_accuracy("Mapper (semi-g: c>=0, a<=0.75) + Seq2seq", preds_map, y)
            # Strict mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.0)
            print_accuracy("Mapper (strict: c>=0, a==0) + Seq2seq", preds_map, y)

def train(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder

    data = get_data_from_file(args.infile,
                              cutoff=(args.max_input, args.max_output),
                              cutoff_mode="truncate")
    if args.validate:
        v_data = get_data_from_file(args.validate)

    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    enc_vectorizer.update(data.characters(0))
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(data.characters(1))
    dec_vectorizer.update(('<START>', '<END>'))

    c_wrapper, w_wrapper = build_model(
        args, enc_vectorizer, dec_vectorizer,
        args.max_input, args.max_output
    )
    if args.twostage:
        X, y = convert_to_xy(data, as_words=True)
        if args.validate:
            X_v, y_v = convert_to_xy(v_data, as_words=True)
            params = {'validation_data': ([X_v, None], [y_v])}
        else:
            params = {'validation_split': args.validation_split}
        params['batch_size'] = args.batch * args.max_sequence
        history = train_model(c_wrapper, [X, None], [y], args, **params)

    X, y = convert_to_xy(data)
    if args.validate:
        X_v, y_v = convert_to_xy(v_data)
        params = {'validation_data': ([X_v, None], [y_v])}
    else:
        params = {'validation_split': args.validation_split}
    history = train_model(w_wrapper, [X, None], [y], args, **params)

    if args.prefix:
        save_model(w_wrapper, args.prefix)
        save_history(history, args.prefix)

    if args.eval is not None:
        w_wrapper.model.decoder = GreedyDecoder()
        for evalfile in args.eval:
            evaluate_model(w_wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.decoders import GreedyDecoder, BeamSearchDecoder
    from mblearn.models.filters import DictionaryFilter
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'word_decoder_output': Wacc}
    )
    if args.decoder == 'greedy':
        wrapper.model.decoder = GreedyDecoder()
    elif args.decoder == 'beam':
        wrapper.model.decoder = BeamSearchDecoder(beam_width=args.width)
    if args.filter:
        words = [w.strip() for w in args.filter.readlines()]
        vectorizer = wrapper.output_vectorizer[wrapper.model.decoder_output[0]]
        eos_idx = wrapper.model.end_symbol[0]
        sys.stderr.write("Building dictionary...")
        dictfilt = DictionaryFilter(words, vectorizer, eos_idx)
        sys.stderr.write(" done.\n")
        wrapper.model.decoder.append_filter(dictfilt)

    evaluate_model(wrapper, args)

def embed(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, HiddenStateLSTM, MaskedMerge, merge, TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    from pprint import pprint

    wrapper = load_model(args.prefix)
    model = wrapper.model

    if not args.words:
        embedding = model.get_layer(name="embedding_1")
        embeds, *_ = embedding.get_weights()
        for n in range(embeds.shape[0]):
            char = wrapper.input_vectorizer[0].get_item(n)
            weights = [str(w) for w in embeds[n]]
            print("\t".join((char, ",".join(weights))))

    else:
        model.compile(loss="sparse_categorical_crossentropy", optimizer="adam")
        enc_in  = model.get_layer(name="encoder_input")
        if enc_in is None:
            enc_in = model.get_layer(name="main_enc_input")
        enc_in = enc_in.input
        enc_out = model.get_layer(name="hiddenstatelstm_1").output
        get_output = K.function([enc_in, K.learning_phase()], enc_out)
        X, _ = wrapper.transform_inputs([list(args.words), None], None)
        embeds, *_ = get_output([X[0], False])
        for n in range(embeds.shape[0]):
            word = str(args.words[n])
            weights = [str(w) for w in embeds[n]]
            print("\t".join((word, ",".join(weights))))


def pratt(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, HiddenStateLSTM, MaskedMerge, merge, TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    if args.plot:
        import matplotlib
        matplotlib.use('Agg')
        import seaborn as sns

    wrapper = load_model(args.prefix)
    for n, layer in enumerate(wrapper.model.layers):
        if isinstance(layer, AttentionLSTM):
            attn_idx = n
            break
    else:
        print(wrapper.model.layers)
        print("Couldn't find attentional layer!")
        exit(1)

    wrapper.model.layers[attn_idx].output_alpha = True
    # hack to make "output_alpha" take effect... there must be a better way
    wrapper.model = load(dump(wrapper.model))
    wrapper.model.load_weights(args.prefix + '.h5')
    wrapper.model = Model(input=wrapper.model.inputs, output=wrapper.model.layers[attn_idx].output)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam"
    )

    print("Generating attention weights from: {0}".format(args.infile.name))
    data = get_data_from_file(args.infile)
    X, y = convert_to_xy(data, for_decoder_input='<START>')
    wrapper.input_vectorizer[1] = wrapper.output_vectorizer[0]
    #wrapper.input_padding = ['left', 'right']
    preds = wrapper.predict([X, y], batch_size=args.batch, verbose=args.verbose)

    for i, (p_i, x_i, y_i) in enumerate(zip(preds, X, y)):
        weight_map = p_i[-len(y_i):, -len(x_i):].T
        if args.csv:
            args.csv.write("\t" + "\t".join(y_i) + "\n")
            for j, x_j in enumerate(x_i):
                args.csv.write(x_j + "\t")
                weight_row = ["{0:.4f}".format(e) for e in weight_map[j]]
                args.csv.write("\t".join(weight_row) + "\n")
        if args.plot:
            ax = sns.heatmap(weight_map[:, ::-1],
                             xticklabels=y_i,
                             yticklabels=x_i[::-1],
                             square=True, robust=True,
                             cbar=False)
            fig = ax.get_figure()
            fig.savefig(args.plot + str(i) + ".png")


if __name__ == "__main__":
    description = "Normalization using encoder-decoder architectures."
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    subparsers = parser.add_subparsers(title="available commands", dest='command')

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--att',
                         action='store_true',
                         default=False,
                         help='Use attentional decoders')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--twostage',
                         action='store_true',
                         default=False,
                         help=('Pre-train a context-free model, then only '
                               'fine-tune using the context model'))
    p_train.add_argument('-D', '--depth-sequence',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Depth of the sequence RNN (default: %(default)i)')
    p_train.add_argument('-R', '--dropout-sequence',
                         metavar='D',
                         type=float,
                         default=0.0,
                         help='Dropout for sequence RNN (default: %(default)f)')
    p_train.add_argument('--max-sequence',
                         metavar='N',
                         type=int,
                         default=5,
                         help='Maximum sequence length (default: %(default)i)')

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-d', '--decoder',
                        choices=('greedy','beam'),
                        default='greedy',
                        help='Decoder to use (default: %(default)s)')
    p_eval.add_argument('-w', '--width',
                        type=int,
                        default=5,
                        help='Beam width when using "--decoder beam" (default: %(default)i)')
    p_eval.add_argument('-f', '--filter',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File with words for a dictionary filter')
    p_eval.add_argument('--print',
                        action='store_true',
                        default=False,
                        help='Print predictions instead of scores')
    p_eval.add_argument('--trainfile',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File the model was trained on; used for extra evaluations')

    p_att = subparsers.add_parser('pratt')
    p_att.set_defaults(func=pratt,
                       help="Print attention weights")
    p_att.add_argument('--csv',
                       metavar='FILE',
                       type=argparse.FileType('w', encoding="UTF-8"),
                       default=sys.stdout,
                       help='Save to file as CSV (default: <stdout>)')
    p_att.add_argument('--plot',
                       metavar='FILEPREFIX',
                       type=str,
                       default=None,
                       help='Save plots as FILEPREFIX{n}.png')
    add_common_eval_params(p_att)

    p_emb = subparsers.add_parser('embed')
    p_emb.set_defaults(func=embed,
                       help="Extract embeddings")
    p_emb.add_argument('-s', '--seed',
                       metavar='N',
                       type=int,
                       default=0,
                       help='Seed for RNG (default: 0=none)')
    p_emb.add_argument('-w', '--words',
                       type=str,
                       nargs='+',
                       help='Words to generate embeddings for')
    p_emb.add_argument('-x', '--prefix',
                       metavar='STR',
                       type=str,
                       required=True,
                       help='Prefix for loading the model (required)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    if args.command == "eval" and args.print:
        args.verbose = 0
    if args.command == "train" and args.att and args.depth > 1:
        sys.stderr.write("Caution: you used attention (--att) with --depth >1\n")
        sys.stderr.write(" -- currently, only the encoder will be deep; the attentional decoder\n")
        sys.stderr.write("    will be a shallow LSTM!\n")

    args.func(args)
