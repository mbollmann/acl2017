#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import sys

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params

def Wacc(*args):
    from mblearn.metrics import sparse_categorical_accuracy_per_sequence
    return sparse_categorical_accuracy_per_sequence(*args)

def convert_to_xy(data, batch_size=None, sentence_input=False):
    if not sentence_input:
        X, y = tuple(data.words(0)), tuple(data.words(1))
    else:
        X = tuple('_'.join(words) for words in data.sentences(0))
        y = tuple('_'.join(words) for words in data.sentences(1))
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def count_correct(preds, truths):
    return sum(''.join(p) == t for p, t in zip(preds, truths))

def evaluate_model(wrapper, args, infile=None):
    if infile is None:
        infile = args.infile
    print("Evaluating on: {0}".format(infile.name))
    data = get_data_from_file(infile)
    X, y = convert_to_xy(data, batch_size=args.batch,
                         sentence_input=args.sentence_input)
    preds = wrapper.predict_classes(X, batch_size=args.batch, verbose=args.verbose)
    correct = count_correct(preds, y)
    print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds), correct / len(preds)))

def train(args):
    from keras.layers import Activation, Embedding, Dense, Dropout
    from keras.layers.wrappers import TimeDistributed
    from keras.optimizers import Adam

    from mblearn.models.seq2seq import get_recurrent_unit, make_simple_seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    vectorizer = Vectorizer(unk_symbol="<UNK>")

    vectorizer.update(data.characters(0))
    vectorizer.update(data.characters(1))
    model = make_simple_seq2seq(
        len(vectorizer),
        (args.max_input, args.max_output),
        args.embedding,
        args.hidden,
        depth=args.depth,
        dropout=args.dropout,
        recurrent_unit=args.recurrent_unit,
        bidirectional=args.bidir
    )
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=Adam(lr=0.003),
        metrics=['accuracy', Wacc]
    )
    wrapper = CategoricalModelWrapper(model=model, vectorizer=vectorizer)

    X, y = convert_to_xy(data, batch_size=args.batch,
                         sentence_input=args.sentence_input)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        params = {'validation_data': convert_to_xy(
            v_data, batch_size=args.batch,
            sentence_input=args.sentence_input)}
    else:
        val_split = (len(X) * args.validation_split) // args.batch * args.batch / len(X)
        params = {'validation_split': val_split}
    history = train_model(wrapper, X, y, args, **params)

    if args.prefix:
        save_model(wrapper, args.prefix)
        save_history(history, args.prefix)

    if args.eval is not None:
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics=['accuracy', Wacc]
    )
    evaluate_model(wrapper, args)


if __name__ == "__main__":
    description = "Normalization using neural network architectures."
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    subparsers = parser.add_subparsers(title="available commands")

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--sentence-input',
                         action='store_true',
                         default=False,
                         help='Use full sentences as input, not words')

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('--sentence-input',
                        action='store_true',
                        default=False,
                        help='Use full sentences as input, not words')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    args.func(args)
