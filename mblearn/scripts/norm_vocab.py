from collections import defaultdict, Counter

class Vocab:
    def __init__(self, X, y):
        self.set_vocab(X, y)

    def set_vocab(self, X, y):
        self.vocab = defaultdict(Counter)
        self.mappings = {}
        for (i_X, i_y) in zip(X, y):
            self.vocab[i_X][tuple(i_y)] += 1
        for (i_X, counter) in self.vocab.items():
            mappings = counter.items()
            total = sum(m[1] for m in mappings)
            best = sorted(mappings, key=lambda m: m[1])[-1]
            ambiguity = 2.0 - (2 * best[1] / total)
            self.mappings[i_X] = (best[0], best[1], ambiguity)

    def predict_with_map(self, X, p, min_count=5, max_ambiguity=0.2):
        """Replace predictions with a direct mapping from the vocabulary.

        Only predictions that have an unambiguous mapping are replaced; the
        others are left unchanged.

        'min_count' is the minimum number of instances that have to be seen of
        the best mapping, 'max_ambiguity' is the maximum allowable ambiguity.
        """
        new_p = []
        for i_X, i_p in zip(X, p):
            new_pred = i_p  # default case
            if i_X in self.vocab:
                (pred, count, ambiguity) = self.mappings[i_X]
                if count >= min_count and ambiguity <= max_ambiguity:
                    new_pred = list(pred)
            new_p.append(new_pred)
        return new_p
