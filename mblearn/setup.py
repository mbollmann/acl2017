from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='mblearn',
      version='0.1',
      description='Machine learning resources, mostly to extend Keras',
      long_description=readme(),
      classifiers=[
          'Development Status :: 2 - Pre-Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3',
      ],
      url='http://github.com/mbollmann/mblearn',
      author='Marcel Bollmann',
      author_email='marcel@bollmann.me',
      license='MIT',
      packages=['mblearn'],
      install_requires=[
          'keras>=1.0.8',
          'numpy>=1.10.0',
          'h5py>=2.0.0',
          'seaborn>=0.7.0',
          'camel>=0.1'
      ],
      setup_requires=[
          'pytest-runner'
      ],
      tests_require=[
          'pytest',
          'hypothesis>=3.1.3'
      ],
      include_package_data=True,
      zip_safe=False)
