#!/usr/bin/python

import argparse

def main(args):
    for line in args.infile:
        data = line.rstrip().split("\\")
        word, phonemes = data[1], data[4]
        if args.reduce:
            phonemes = phonemes.replace("'", "").replace("-", "")
        print("\t".join((word, phonemes)))


if __name__ == '__main__':
    description = ""
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('infile',
                        nargs='?',
                        type=argparse.FileType('r'),
                        help='CELEX file')
    parser.add_argument('-r', '--reduce',
                        action='store_true',
                        default=False,
                        help='Reduce phonemes by removing stress and syllable boundary markers')

    args = parser.parse_args()
    main(args)
